/***********************************************************************
 *
 * Copyright (C) 2010-2017 Innocent De Marchi <tangram.peces@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ***********************************************************************/
#include <QtGui>
#include <QTimer>
 #include <math.h>

#include "rellotge.h"

Rellotge::Rellotge(QWidget * parent , Qt::WindowFlags)
       : QLabel(parent)
{
    rellotgeQTimer = new QTimer();
    connect(rellotgeQTimer, SIGNAL(timeout()), this, SLOT(actualitzaRellotge()));
    p_rellotgeEnMarxa=false;
    hores=0;
    minuts=0;
    segons=0;
}

void Rellotge::actualitzaRellotge(){
//    milisegons10++;
    segons=segons+1;
//    if (milisegons10>=100){
//        milisegons10=milisegons10-100;
//        segons=segons+1;
//    }
    if (segons>=60){
        segons=segons-60;
        minuts=minuts+1;
    }
    if (minuts>=60){
        minuts=minuts-60;
        hores=hores+1;
    }
    setText(retornaTemps());
}

void Rellotge::iniciaRellotge(){
  p_rellotgeEnMarxa= true;
  setText(retornaTemps());
  rellotgeQTimer->start(1000);
}

//Fa que el rellotge aturat segueixi en marxa
void Rellotge::reIniciaRellotge(){
  rellotgeQTimer->start(1000);
  p_rellotgeEnMarxa= true;
  estableixTemps(retornaTemps());
  setText(retornaTemps());
}

void Rellotge::aturaRellotge(){
  rellotgeQTimer->stop();
  p_rellotgeEnMarxa=false;
  setText(retornaTemps());
  update();
}

void Rellotge::estableixTemps(QString temps){
   QStringList list1 = temps.split(":");
   hores=list1.at(0).toInt();
   minuts=list1.at(1).toInt();
   segons=list1.at(2).toInt();
   setText(retornaTemps());
   update();
}

QString Rellotge::retornaTemps(int tipus){

    QString sHores,sMinuts,sSegons;

    if (hores<10){
      sHores= QString("0%1:").arg(hores);
    }
    else sHores= QString("%1:").arg(hores);

    if (minuts<10){
      sMinuts= QString("0%1:").arg(minuts);
    }
    else sMinuts= QString("%1:").arg(minuts);

    if (segons<10){
      sSegons= QString("0%1").arg(segons);
    }
    else sSegons= QString("%1").arg(segons);

if (tipus==0){
    if(rellotgeQTimer->isActive()){
       return QString("<h1><font color=red>%1%2%3</font></h1>").arg(sHores,sMinuts,sSegons);
    }
    else {
        return QString("<h1><font color=blue>%1%2%3</font></h1>").arg(sHores,sMinuts,sSegons);
    }
}
else  {
    return QString("%1%2%3").arg(sHores,sMinuts,sSegons);
  }
}

bool Rellotge::rellotgeEnMarxa(){
    return p_rellotgeEnMarxa;
}

/*Retorna el temps marcat en segons totals
  No es fa servir*/
int Rellotge::retornarSegonsTotals(){
  return hores*3600+minuts*60+segons;
}
