/***********************************************************************
 *
 * Copyright (C) 2010-2017 Innocent De Marchi <tangram.peces@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ***********************************************************************/
#include <QtCore>
#include <QtGui>
#include <QtWidgets/QProgressDialog>

#include "tauler.h"
#include "fitxa.h"
#include "frmprincipal.h"
#include "moviment.h"
#include "rellotge.h"
#include "funcions.h"
#include "constants.h"

/****************************************
*
*15/12/10: S'ha canviat el sceneJoc: ara, cada
* vegada que es canvia la modalitat de joc,
* es crea un sceneJoc. D'aquesta manera, el joc
* queda sempre ben dimensionat.
*****************************************/


const double distanciaEntreFitxes = 1.2;

extern int jocResolt;


int jocsSenseSolucio=0;
QString darrerEscriuMoviments;

//QList<QPoint> llistaCoordenadesFitxesActives;
//QList<int> llistaNumeroConjuntFitxesActives;

Tauler::Tauler(  QUndoStack *movimentsUndoStack , Rellotge *rellotge,frmPrincipal *frmPrincipal,  QWidget* parent )
  : QGraphicsView(parent),
  p_movimentsUndoStack(movimentsUndoStack),
  p_rellotge(rellotge),
  p_frmPrinci(frmPrincipal)
{
        QGraphicsScene* sceneJoc = new QGraphicsScene(this);
        sceneJoc = new QGraphicsScene(this);
        setScene(sceneJoc);
        setRenderHints(QPainter::Antialiasing | QPainter::TextAntialiasing);
        setFrameStyle(QFrame::NoFrame);
        setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        //setMinimumSize(frmPrincipal->minimumSize());
        setMinimumSize(QSize(290,290));

        comptadorFitxesJoc=0;
        comptadorFitxesEstatZeroJoc=0;
        //inicialment es juga
        p_estatDelJoc=joc;
        p_SolitariAtzar=false;
        p_SolitariPersonalitzat=false;
}

/*
  Retorna l'estat de la fitxa amb les coordenades del paràmetre
  */
int Tauler::estatFitxaJoc( const  QPoint& coordenades) const {
    if (m_fitxes.contains(coordenades.x()*100+coordenades.y())){
       return m_fitxes.value(coordenades.x()*100+coordenades.y())->estat();
           }
   else return -1;
}

void Tauler::marcaMovimentsFitxa(QList<QPoint> movimentsFitxa){
    for (int i=0; i<movimentsFitxa.size();++i){
          m_fitxes.value(movimentsFitxa.at(i).x()*100+movimentsFitxa.at(i).y())->setEstat(3);
    }
}

 //Canvia l'estat de les fitxes per tal de marcar un moviment
void Tauler::marcaMoviment (QString moviment){
    QStringList moviments=moviment.split(" ");
    m_fitxes.value(moviments.value(0).toInt())->setEstat(2);
    m_fitxes.value(moviments.value(2).toInt())->setEstat(3);
}

/*
  Elimina els buits marcats com a final d'un moviment anterior
  */
void Tauler::eliminaMarquesMoviments(const  QPoint& coordenades){
    //QHashIterator <int, Fitxa*> i(m_fitxes);
    QMapIterator <int, Fitxa*> i(m_fitxes);
    while (i.hasNext()) {
        i.next();
        if (i.value()->estat()==3){
            i.value()->setEstat(0);
        }
        if(  (i.value()->estat()==2) && !(i.key()==(coordenades.x()*100+coordenades.y())) ){
            i.value()->setEstat(1);
        }
    }
}


void Tauler::ferMoviment (int fitxaInicial, int fitxaBotada, int fitxaFinal,bool missatge){
    Moviment* nouMoviment = new Moviment(fitxaInicial,fitxaBotada,fitxaFinal,p_tipusMoviment,this);
    /*qCritical("%s", qPrintable(QString("%1 %2 %3").
                                  arg(fitxaInicial).
                                  arg(fitxaBotada).
                                  arg(fitxaFinal)));*/
    p_movimentsUndoStack->push(nouMoviment);
    p_frmPrinci->iniciSolucioAction->setEnabled(p_movimentsUndoStack->canUndo());
    //NO es mostrarà el missatge en els solitaris
    //a l'atzar
    if( (p_estatDelJoc==joc) && missatge){
        p_frmPrinci->mostraMissatge(3);
    }
    //  p_frmPrinci->statusBar()->showMessage(missatgeNumeroMoviments());;}
    //Cal els segon true per a què s'aturi el rellotge en
    //haver acabat el joc quan es juga manualment
    marcaFinalDeJoc(p_tipusMoviment,true,true);
//    qDebug("moviment");
    //?????
    ocuparFitxesFinalJoc();
}

bool Tauler::ferMoviment2(int fitxaInicial, int fitxaBotada, int fitxaFinal,bool missatge){

   if( (m_fitxes.value(fitxaInicial)->estat()==1 &&
      m_fitxes.value(fitxaBotada)->estat()==p_estatFitxaMitjana  &&
      m_fitxes.value(fitxaFinal)->estat()==0 )
               ){
       Moviment* nouMoviment = new Moviment(fitxaInicial,fitxaBotada,fitxaFinal,p_tipusMoviment,this);
       /*qCritical("%s", qPrintable(QString("%1 %2 %3").
                                  arg(fitxaInicial).
                                  arg(fitxaBotada).
                                  arg(fitxaFinal)));*/
       p_movimentsUndoStack->push(nouMoviment);
       p_frmPrinci->iniciSolucioAction->setEnabled(p_movimentsUndoStack->canUndo());
       //NO es mostrarà el missatge en els solitaris
       //a l'atzar
       if( (p_estatDelJoc==joc) && missatge){
           p_frmPrinci->mostraMissatge(3);
       }
         //  p_frmPrinci->statusBar()->showMessage(missatgeNumeroMoviments());;}
       marcaFinalDeJoc(p_tipusMoviment,true);
       //?????
       ocuparFitxesFinalJoc();
       return true;
    }
return false;
}

void Tauler::marcaFinalDeJoc(int direccioMoviment, bool marcaFinal, bool aturaRellotge){
    if( jocFinalitzat()){
        //QHashIterator <int, Fitxa*> i(m_fitxes);
        QMapIterator <int, Fitxa*> i(m_fitxes);
        int numFitxes=0, ordFitxa=0;
        int numFitxesFinalJoc=0;
        jocResolt=1;
        if(p_estatDelJoc==joc){
//            qDebug("joc");
            if(aturaRellotge){
//                qDebug("atura rellotge");
            p_rellotge->aturaRellotge();
            }
            p_frmPrinci->mostraMissatge(0);
        }
//        qDebug("joc finalitzat sense solució");
        //es posen les fitxes en color vermell
        while (i.hasNext()) {
              i.next();
              if (i.value()->estat()==1){
                  i.value()->setEstat(5);
                  numFitxes++;
                  ordFitxa=i.key();
                  if(i.value()->fitxaFinalJoc()){
                   numFitxesFinalJoc++;
                  }
              }
        }

        //En les modalitats "inverses" no queda només una fitxa
        //Aqui es controla quan queda només una fitxa o
        //bé els casos en què hi ha fitxes marcades
        //com a final del joc
        if ( ( (numFitxes==1) && (direccioMoviment != 2)
               && comptadorFitxesFinalJoc==0 )
            || (nomesQuedenFitxesMarcadesFinalJoc() && (direccioMoviment != 2) )
                                    ) {
          jocResolt=2;
          if(aturaRellotge){
          p_rellotge->aturaRellotge();
          }
          m_fitxes.value(ordFitxa)->setEstat(4);
          i.toFront();
          while (i.hasNext()) {
              i.next();
              if (i.value()->estat()==5){
                  i.value()->setEstat(4);
              }
          }
          p_frmPrinci->mostraMissatge(1);
          if(p_estatDelJoc==joc){
              controlaRecords();
              p_frmPrinci->desaSolucions1();
              p_frmPrinci->veureSolucionsSlot();}
        }

        /* La primera opció és pels jocs que començan amb una fitxa.
         * La segona, per aquells que començan amb més d'una fitxa
         * (i això és per a les modaltats inverses)
        */
          if ( ((numFitxes==numeroDeMovimentsJoc()+1) && (direccioMoviment == 2)
              && comptadorFitxesJoc==1)  ||
             ((numFitxes==numeroDeMovimentsJoc()+comptadorFitxesJoc) &&
             (direccioMoviment == 2)) ){
          i.toFront();
          while (i.hasNext()) {
              i.next();
              if (i.value()->estat()==5){
                  i.value()->setEstat(4);
              }
          }
          if(aturaRellotge){
          p_rellotge->aturaRellotge();
          }
          jocResolt=2;
          p_frmPrinci->mostraMissatge(1);
          if(p_estatDelJoc==joc){
              controlaRecords();
              p_frmPrinci->desaSolucions1();
              p_frmPrinci->veureSolucionsSlot();}
        }
     //14/12/12 Evita que quedin les fitxes bloquejades quan NO es
     //resol el joc
     //03/01/12  què????
     //04/01/12 S'ha deixat que quedin bloquejades les fitxes quan
     //s'hagi resolt el joc
     //if (jocResolt==2){
       //  setInteractive(false);}

     /*si es tracta d'un solitari personalitzat,
      *cal comprovar si s'ha de desar
      */
     if( (solitariPersonalitzat()) && (jocResolt==2) &&
         (p_estatDelJoc==joc) && marcaFinal){
         p_frmPrinci->solitariPersonalitzat_desarSolitari();
     }


//    solucionaJocActual_CalculaArbreJoc_escriuMoviments();
    }// final if jocFinalitzat()

}

/*
 * Comprova si només queden fitxes marcades com a final
 * de joc (i estan activades!)
 */
bool Tauler::nomesQuedenFitxesMarcadesFinalJoc(){
  //QHashIterator <int, Fitxa*> i(m_fitxes);
  QMapIterator <int, Fitxa*> i(m_fitxes);
  bool nomesFinalJoc=true;
  while (i.hasNext()) {
        i.next();
       // if ( ( ( (i.value()->estat()==1) || (i.value()->estat()==5) ) &&
        if ( (  (i.value()->estat() != 0) &&
             (!i.value()->fitxaFinalJoc())
                                   ) ||
            ((i.value()->estat()==0) &&
                (i.value()->fitxaFinalJoc()))
                                            ) {
             nomesFinalJoc=false;
       }
   }
  return nomesFinalJoc;
  }

/*
  Comprova, desprès de cada moviment, si el joc s'ha acabat
  El joc s'ha acabat si només queda una fitxa (o totes en
  les modalitats inverses)
  si només queden les fitxes (ocupades) marcades
  com a final de joc
  o si les fitxes que queden no son final de joc
  i no es poden moure
  */
bool Tauler::jocFinalitzat(){

   //QHashIterator <int, Fitxa*> i(m_fitxes);
   QMapIterator <int, Fitxa*> i(m_fitxes);
   //Pressuposam que el joc ha acabat
   bool finalitzat=true;
   while (i.hasNext()) {
        i.next();
        //Només comprovam les fitxes actives!
        if ( (i.value()->estat()==1) ){
            /* Si la fitxa es pot moure i
             * no està marcada com a final
             * del joc-> el joc no ha acabat
             */
            if  (i.value()->esPotMoure() &&
                !i.value()->fitxaFinalJoc()){
                finalitzat=false;
                break;
             }

            /* La fitxa es pot moure, però està marcada com
             * a final de joc i queden altres fitxes que
             * no estan marcades com a final de joc
             */
            if (
                 i.value()->esPotMoure() &&
                 i.value()->fitxaFinalJoc() &&
                 !nomesQuedenFitxesMarcadesFinalJoc()
                                   ){
                finalitzat=false;
                break;
                }
            if(p_tipusMoviment==2 && i.value()->esPotMoure()){
                finalitzat=false;
                break;
            }
            if( i.value()->fitxaFinalJoc() &&
                !nomesQuedenFitxesMarcadesFinalJoc()){
              finalitzat=false;
              break;
            }
        }
    }
 return finalitzat;
}

void Tauler::controlaRecords(){
    //dels solitaris especials no es desen els records
    if( !esSolitariEspecial()){
    QSettings records;
    QString record=records.value(QString("%1/record").arg(p_modalitatJocActual),QString("100:00:00")).toString();
    if (p_rellotge->retornaTemps(1)< record){
        QSettings().setValue(QString("%1/record").arg(p_modalitatJocActual), p_rellotge->retornaTemps(1));
        //missatge nou record personal
        p_frmPrinci->mostraMissatge(2);
    }
  }
  //  else qDebug("és una solitari especial! %d",p_modalitatJocActual.toInt());
}

/*
Carrega les fitxes de la modalitat de joc passada per paràmetre
  */
void Tauler::joc_CarregaJoc( QString p_tipusJoc, QString p_filesColumnes, QString p_estatFitxes){
    /*
     *Això fa que es «redimensioni» correctament el tauler
     *de joc quan es canvia de modalitat
     */
    scene()->clear();
    QGraphicsScene* sceneJoc1 = new QGraphicsScene(this);
    setScene(sceneJoc1);

    p_rellotge->aturaRellotge();
    p_rellotge->estableixTemps(QString("00:00:00"));
    comptadorFitxesJoc=0;
    comptadorFitxesEstatZeroJoc=0;
    p_estatDelJoc=joc;

/*qCritical("p_tipusJoc %s", qUtf8Printable(p_tipusJoc));
qCritical("p_filesColumnes %s", qUtf8Printable(p_filesColumnes));
qCritical("p_estatFitxes %s", qUtf8Printable(p_estatFitxes));*/

    QStringList  tipusJoc = p_tipusJoc.split(" ");
    setModalitatJocActual(tipusJoc.value(0));
    //Comprovam els solitaris especials (atzar i personalitzats)
    //setSolitariEspecial(p_modalitatJocActual);
    QStringList  filesColumnes= p_filesColumnes.split(" ");
    QStringList  estatFitxes = p_estatFitxes.split(" ");
    p_tipusMoviment=tipusJoc.value(1).toInt();
    p_numFitxesAillades=tipusJoc.value(2).toInt();
    p_numMoviments=tipusJoc.value(3).toInt();
    if ( (p_tipusMoviment==2) || (p_tipusMoviment==5) ){p_estatFitxaMitjana=0;}
    else {p_estatFitxaMitjana=1;}

    int files, columnes;
    files= filesColumnes.value(0).toInt();
    columnes=filesColumnes.value(1).toInt();
    m_fitxes.clear();
    if (columnes>0){
        for(int j=0 ; j<files;++j){
              for(int i=0 ; i<columnes;++i){
                   if(estatFitxes.value(j*columnes+i).toInt()>-1){
                       QPoint coord(j,i); //fila i columna
                       Fitxa *fitxa= new Fitxa(this);
                       fitxa->setCoordenades(coord);
                       fitxa->setEstat(estatFitxes.value(j*columnes+i).toInt());
                      // qDebug("%d",estatFitxes.value(j*columnes+i).toInt());
                       /*qCritical("%s", qPrintable(QString("Contador %1 estatFitxa %2")
                                                  .arg(estatFitxes.value(j*columnes+i).toInt())
                                                  .arg(estatFitxes.value(j*columnes+i).toInt())));*/
                       //fitxa->setFitxaSolitariPersonalitzat(false);
                       fitxa->setTipusMoviment(tipusJoc.value(1).toInt());
                       fitxa->setPos(fitxa->coordenades().y()*distanciaEntreFitxes*30,
                                             fitxa->coordenades().x()*distanciaEntreFitxes*30);
                       m_fitxes.insert(coord.x()*100+coord.y(),fitxa);
                       //qDebug("Insertada fitxa %d",coord.x()*100+coord.y());
                       //configuracioActual(false);
                       scene()->addItem(fitxa);
                       if (fitxa->estat()==1) {++comptadorFitxesJoc;}
                       if (fitxa->estat()==0){++comptadorFitxesEstatZeroJoc;}
                   }//if estatFitxes
               } //for columnes
           }//for files
 }
    else { //Solitaris de tipus triangular
    int contador=0;
    for(int j=0 ; j<files;++j){
               for(int i=0; i<j;++i){
                   //Afegit el if pel solitari tipus Rombe
                   if (estatFitxes.value(contador).toInt()>-1){
                       QPoint coord(j-1,i); //fila i columna
                       Fitxa *fitxa= new Fitxa(this);
                       fitxa->setEstat(estatFitxes.value(contador).toInt());
                      /* qCritical("%s", qPrintable(QString("Contador %1 estatFitxa %2")
                                                  .arg(contador)
                                                  .arg(estatFitxes.value(contador).toInt())));*/
                       fitxa->setTipusMoviment(tipusJoc.value(1).toInt());
                       fitxa->setCoordenades(coord);
                       fitxa->setPos( fitxa->coordenades().y()*distanciaEntreFitxes*30-15*1.2*j,
                                             fitxa->coordenades().x()*distanciaEntreFitxes*30+4*j);
                    //   fitxa->setPos( fitxa->coordenades().y()*distanciaEntreFitxes*30-15*1.2*j,
                     //                        fitxa->coordenades().x()*distanciaEntreFitxes*30);
                     //  qDebug("fitxa %d,%d",fitxa->pos().x(),fitxa->pos().y());
                       m_fitxes.insert(coord.x()*100+coord.y(),fitxa);
                       scene()->addItem(fitxa);
                       if (fitxa->estat() == 1) {++comptadorFitxesJoc;}
                       if (fitxa->estat()==0){++comptadorFitxesEstatZeroJoc;}
                        }//if estat fitxa
                       ++contador;
               } //for columnes
           }//for files
    }
p_movimentsUndoStack->clear();
configuracioActual(false);
setInteractive(true);
//Això per a què s'ajusti a la finestra
fitInView(sceneRect(), Qt::KeepAspectRatio);
}

/*Controla els tipus de joc especials
 */
void Tauler::setSolitariEspecial(QString codiJoc){
  //pressuposam que no és un solitari especial
  setSolitariAtzar(false);
  setSolitariPersonalitzat(false);
  //Marcam els jocs a l'atzar
  setSolitariAtzar(codiJoc=="1000");
  //marcam els jocs personalitzats
  setSolitariPersonalitzat(
   codiJoc=="1001" || codiJoc=="1002" ||codiJoc=="1003" );
}

/*
 * En els jocs amb fitxes finals marcades,
 * controla si el número de moviments per acabar
 * el joc permet deixar les fitxes finals ocupades
 */
bool Tauler::ocuparFitxesFinalJoc(){
  bool resultat=true;
  /* Si és un joc amb fitxes marcades com a final del joc
   */
  //QHashIterator <int, Fitxa*> fitxesFinalsJoc(m_fitxes);
  QMapIterator <int, Fitxa*> fitxesFinalsJoc(m_fitxes);
  bool ambFitxesFinalJoc=false;
  while (fitxesFinalsJoc.hasNext()) {
       fitxesFinalsJoc.next();
       if ( (fitxesFinalsJoc.value()->fitxaFinalJoc()) ){
         ambFitxesFinalJoc=true;
       }
  }
  if(ambFitxesFinalJoc){
   resultat=( (numeroFitxesFinalsSenseOcupar()<4) &&
              (numeroFitxesPerEliminar()>numeroFitxesFinalsSenseOcupar()) );
  }
  return resultat;
}

/* Retorna el número de fitxes marcades com a final
 * del joc que estan sense ocupar
 */
int Tauler::numeroFitxesFinalsSenseOcupar(){
    //QHashIterator <int, Fitxa*> fitxesFinalsJoc(m_fitxes);
    QMapIterator <int, Fitxa*> fitxesFinalsJoc(m_fitxes);
    int numeroFitxesFinalJoc=0;
    while (fitxesFinalsJoc.hasNext()) {
         fitxesFinalsJoc.next();
         if ( (fitxesFinalsJoc.value()->fitxaFinalJoc()) &&
             fitxesFinalsJoc.value()->estat()==0){
           numeroFitxesFinalJoc++;
         }
    }
   return numeroFitxesFinalJoc;
}

/* Calcula quantes fitxes cal eliminar per acabar
 * el joc (fitxes actives que no estan marcades com
 * a final de joc)
 */
int Tauler::numeroFitxesPerEliminar(){
    //QHashIterator <int, Fitxa*> fitxesFinalsJoc(m_fitxes);
    QMapIterator <int, Fitxa*> fitxesFinalsJoc(m_fitxes);
    int numeroFitxesFinalJoc=0;
    while (fitxesFinalsJoc.hasNext()) {
         fitxesFinalsJoc.next();
         if ( !(fitxesFinalsJoc.value()->fitxaFinalJoc()) &&
             fitxesFinalsJoc.value()->estat()==1){
           numeroFitxesFinalJoc++;
         }
    }
   return numeroFitxesFinalJoc;
}

 void Tauler::resizeEvent(QResizeEvent* event) {
        fitInView(sceneRect(), Qt::KeepAspectRatio);
        QGraphicsView::resizeEvent(event);
}

 /*
   Per a cada fitxa del joc, calcula els moviments possibles
   i els posa en el QList movimentsPossibles de cada fitxa
   */
 void Tauler::solucionaJocActual_CalculaMovimentsPosibles(){
      //Per a cada fitxa del joc, calcula els possibles moviments
     //i es desen a la QList fitxa.movimentsPossibles
     //QHashIterator <int, Fitxa*> iterator_fitxes(m_fitxes);
     QMapIterator <int, Fitxa*> iterator_fitxes(m_fitxes);
     while (iterator_fitxes.hasNext()) {
        iterator_fitxes.next();
        iterator_fitxes.value()->calculaMovimentsPosiblesFitxa();
     }
 }

 /* Comprova els moviments possibles, els selecciona segons diversos
  * criteris i preferències, i retorna una llista amb els moviments
  * preferits
  * Ho fa servir el procediment de cerca de solucions v. 2.1
  */

 QStringList Tauler::solucionaJocActual_CarregaMovimentsArbre1
 (QStringList movimentsFets, QString darrerMoviment){

     //QHashIterator <int, Fitxa*> iterator_fitxes(m_fitxes);
     QMapIterator <int, Fitxa*> iterator_fitxes(m_fitxes);
     QStringList itemsArbre; //tots els moviments possibles
     QStringList itemsArbre1;//moviments que enllaçan amb l'anterior
     QStringList itemsArbre2;//moviments per preferència
     QStringList itemsArbre3;//moviments per preferència que enllaçan amb l'anterior
     QStringList itemsArbre4;//moviments per nivell d'aïllament de les fitxes
     QStringList itemsArbre5;//moviments pel número de moviments posteriors
     QStringList itemsArbre6;//moviments que cumpleixen almenys 3 requisits

     solucionaJocActual_FitxesAillades2();
     /* Número de moviments que es faran al principi a l'atzar*/
     int numeroMovimentsInicialsAtzar=qrand() % comptadorFitxesJoc;
     // int numeroMovimentsInicialsAtzar=round(comptadorFitxesJoc*0.4);
     //qDebug("numeroMovimentsInicialsAtzar %d", numeroMovimentsInicialsAtzar);
     int preferenciaMenor=200;
     int aillamentMajor=0;
     while (iterator_fitxes.hasNext()) {
         iterator_fitxes.next();
         int index_fitxaInicial=
                 iterator_fitxes.value()->coordenades().x()*100+
                 iterator_fitxes.value()->coordenades().y();
         //només comprovam les fitxes actives
         if ( m_fitxes.value(index_fitxaInicial)->estat()==1){
             int movimentsPosibles=0;
             //qDebug("movimentsPossibles.count() %d",iterator_fitxes.value()->movimentsPossibles.count());
             for(int j=0 ; j<iterator_fitxes.value()->movimentsPossibles.count();++j){
                 int index_fitxaBotada=iterator_fitxes.value()->movimentsPossibles.at(j).x();
                 int index_fitxaFinal=iterator_fitxes.value()->movimentsPossibles.at(j).y();
                 //qDebug("p_estatFitxaMitjana %d ",p_estatFitxaMitjana);
                 if (   (m_fitxes.value(index_fitxaBotada)->estat()==p_estatFitxaMitjana) &&
                        (m_fitxes.value(index_fitxaFinal)->estat()==0) ){
                     QString moviment=QString("%1 %2 %3").arg(
                                 index_fitxaInicial).arg(
                                 index_fitxaBotada).arg(
                                 index_fitxaFinal);
                     //és possible aquest moviment
                     if ( (movimentsFets.indexOf(QString("%1 %2").arg(p_movimentsUndoStack->index()+1).
                                                 arg(moviment))==-1)
                          //Aquesta condició no és eficient 19/12/11
                          // && (solucionaJocActual_CalculaMovimentsPosteriors(moviment))
                          ){
                         itemsArbre.append(moviment);
                         ++movimentsPosibles;
                         /*Això s'ha posat una mica més abaix.
                       La intenció és aconseguir més diversitat en els primers moviments
                       del joc */
                         /*if (p_movimentsUndoStack->index()<numeroMovimentsInicialsAtzar){
                         qDebug("Condició itemsArbre.count() %d", itemsArbre.count());
                         return itemsArbre;}*/
                     }
                 }
             } //for
             if (movimentsPosibles>0){
                 //   m_fitxes.value(index_fitxaInicial)->setPreferenciaMoure(movimentsPosibles);
                 preferenciaMenor=qMin(preferenciaMenor,m_fitxes.value(index_fitxaInicial)->preferenciaMoure());
                 aillamentMajor=qMax(aillamentMajor,m_fitxes.value(index_fitxaInicial)->nivellAillament());
             }
         }//if inicial estat==1
     }

     /* Si no hi ha moviments possibles, no val la pena perdre el temps!*/
     if(itemsArbre.count()<1){return itemsArbre;}
     /* Per als primers moviments del joc, no es seleccionan tant els moviments */
     if (p_movimentsUndoStack->index()<numeroMovimentsInicialsAtzar){
         return itemsArbre;}

     //Miram quins moviments maximitzen el número de moviments posteriors
     itemsArbre5=solucionaJocActual_CalculaNumeroMovimentsPosteriors(itemsArbre);

     //Miram si és possible enllaçar algun dels moviments amb el darrerMoviment
     QStringList fitxesDarrerMoviment=QStringList(darrerMoviment.split(" "));
     if (fitxesDarrerMoviment.count()>0){
         //  QString fitxaFinalDarrerMoviment=fitxesDarrerMoviment.value(fitxesDarrerMoviment.count()-1);
         for(int j=0 ; j<itemsArbre.count();++j){
             QStringList fitxesMovimentArbre=QStringList(itemsArbre.value(j).split(" "));
             //final actual i inicial anterior
             if (   ( fitxesMovimentArbre.value(2).contains(fitxesDarrerMoviment.value(0)) ) ||
                    //final actual i mitjana anterior
                    ( fitxesMovimentArbre.value(2).contains(fitxesDarrerMoviment.value(1)))   ||
                    //mitjana actual i final anterior
                    ( fitxesMovimentArbre.value(1).contains(fitxesDarrerMoviment.value(2)))  ||
                    //inicial actual i final anterior
                    ( fitxesMovimentArbre.value(0).contains(fitxesDarrerMoviment.value(2)))  ){
                 itemsArbre1.append(itemsArbre.value(j));
                 //  if (p_movimentsUndoStack->index()>numeroMovimentsInicialsAtzar ){return itemsArbre1;}

                 if (m_fitxes.value(fitxesMovimentArbre.value(0).toInt())->preferenciaMoure()==preferenciaMenor){
                     itemsArbre3.prepend(itemsArbre.value(j));
                     //  if (p_movimentsUndoStack->index()>numeroMovimentsInicialsAtzar ){return itemsArbre3;}
                 }
             }
             if (m_fitxes.value(fitxesMovimentArbre.value(0).toInt())->preferenciaMoure()==preferenciaMenor ){
                 itemsArbre2.prepend(itemsArbre.value(j));
                 // if (p_movimentsUndoStack->index()>numeroMovimentsInicialsAtzar ){return itemsArbre2;}
             }

             if (m_fitxes.value(fitxesMovimentArbre.value(0).toInt())->preferenciaMoure()==aillamentMajor ){
                 itemsArbre4.prepend(itemsArbre.value(j));
                 //  if (p_movimentsUndoStack->index()>numeroMovimentsInicialsAtzar ){return itemsArbre4;}
             }
         }//for
     }// if fitxesDarrerMoviment.count()


     for(int j=0 ; j<itemsArbre.count();++j){
         int repeticio=0;
         if (itemsArbre1.contains(itemsArbre.value(j))) {++repeticio;}
         if (itemsArbre2.contains(itemsArbre.value(j))) {++repeticio;}
         if (itemsArbre3.contains(itemsArbre.value(j))) {++repeticio;}
         if (itemsArbre4.contains(itemsArbre.value(j))) {++repeticio;}
         if (itemsArbre5.contains(itemsArbre.value(j))) {++repeticio;}
         if (repeticio>2){itemsArbre6.append(itemsArbre.value(j));}
     }
     //qDebug("itemsArbre6.count() %d", itemsArbre6.count());
     QStringList movimentsSelecionats;
     /*  versió 2.0 moviments que cumpleixen més de 3 requisits
           if (itemsArbre6.count()>0){return itemsArbre6;}
           //moviments per nivell d'aïllament de les fitxes
           else if (itemsArbre4.count()>0){return itemsArbre4;}
           //moviments per preferència que enllaçan amb l'anterior
           else if (itemsArbre3.count()>0){return itemsArbre3;}
           //moviments pel número de moviments posteriors
           else  if (itemsArbre5.count()>0){return itemsArbre5;}
           //moviments que enllaçan amb l'anterior
           else  if (itemsArbre1.count()>0){return itemsArbre1;}
           //moviments per preferència
           else if (itemsArbre2.count()>0){return itemsArbre2;}
           else return itemsArbre;*/


     /*15/09/16 No està clar quina opció és millor*/
     movimentsSelecionats.append(itemsArbre5);
     movimentsSelecionats.append(itemsArbre2);
     movimentsSelecionats.append(itemsArbre6);
     if (movimentsSelecionats.count()<1){movimentsSelecionats.append(itemsArbre);}


     /*
         QStringList itemsArbre; //tots els moviments possibles
         QStringList itemsArbre1;//moviments que enllaçan amb l'anterior
         QStringList itemsArbre2;//moviments per preferència
         QStringList itemsArbre3;//moviments per preferència que enllaçan amb l'anterior
         QStringList itemsArbre4;//moviments per nivell d'aïllament de les fitxes
         QStringList itemsArbre5;//moviments pel número de moviments posteriors
         QStringList itemsArbre6;//moviments que cumpleixen almenys 3 requisits
     */

     //Ara comprova si hi ha algun moviment que te com a final una fitxa
     //marcada com a final de joc
     if(comptadorFitxesFinalJoc>0){
         // qDebug("movimentsSelecionats.count() %d",movimentsSelecionats.size());
         QStringList movimentsAmbFitxaFinalJoc;
         for(int v=0 ; v<movimentsSelecionats.count();++v){
             //qDebug("EN EL FOR");
             QStringList fitxes=
                     QStringList(movimentsSelecionats.value(v).split(" "));
             if (m_fitxes.value(
                         fitxes.value(2).toInt())->fitxaFinalJoc()){
                 //qDebug("FITXA FINAL!");
                 movimentsAmbFitxaFinalJoc.append(movimentsSelecionats.value(v));
             }
         }
         if(movimentsAmbFitxaFinalJoc.count()>0){
             //qDebug("*** hi ha moviments finals");
             movimentsSelecionats.clear();
             movimentsSelecionats.append(movimentsAmbFitxaFinalJoc);
         }
         //else qDebug("no hi ha moviments finals");
     }
     /*
      qDebug("itemsArbre1.count() %d", itemsArbre1.count());
      qDebug("itemsArbre2.count() %d", itemsArbre2.count());
      qDebug("itemsArbre3.count() %d", itemsArbre3.count());
      qDebug("itemsArbre4.count() %d", itemsArbre4.count());
      qDebug("itemsArbre5.count() %d", itemsArbre5.count());
      qDebug("itemsArbre6.count() %d", itemsArbre6.count());
      qDebug("movimentsSelecionats.count() %d", movimentsSelecionats.count());
      qDebug("*****************");*/
     return movimentsSelecionats;
}


//No es fa servir
//QStringList Tauler::solucionaJocActual_CarregaMovimentsArbre3
//                     (int numeroInicialFitxesJoc,
//                      variables_recerca_solucio variablesSolucio){

//     QMapIterator <int, Fitxa*> iterator_fitxes(m_fitxes);
//     QStringList itemsArbre1,itemsArbre4; //tots els moviments possibles
//     QStringList redueixConjuntsAillades, redueixAillades;

//     QList<int> llistaMovimentsPerConjunt,//conserva el nombre total de moviments de cada conjunt de fitxes
//                llistaDeNumFitxesPerConjunt;

//     solucionaJocActual_CalculaMovimentsPosibles();

//     int numeroDeConjuntsActual=solucionaJocActual_FitxesAillades_PerConjunts();
//     for(int c=0 ; c<numeroDeConjuntsActual;++c){
//      llistaMovimentsPerConjunt.append(0);
//      llistaDeNumFitxesPerConjunt.append(0);
//     }

//     int numeroDeFitxesAilladesActual=solucionaJocActual_numeroFitxesAillades();
//    /* if(p_movimentsUndoStack->index()>-1){
//       qDebug("moviment %d  conjunts %d  aillades %d",
//              p_movimentsUndoStack->index(),numeroDeConjuntsActual,numeroDeFitxesAilladesActual);
//     }*/

//     while (iterator_fitxes.hasNext() && (numeroDeConjuntsActual<=variablesSolucio.maximNumeroDeConjunts
//                            && numeroDeFitxesAilladesActual<=variablesSolucio.maximNumeroDeFitxesAillades)
//            && solucionaJocActual_conjuntsDispersos(llistaMovimentsPerConjunt.count())<2
//                  ) {
//             iterator_fitxes.next();
//             if ( iterator_fitxes.value()->estat()==1){
//             int index_fitxaInicial=
//                 iterator_fitxes.value()->coordenades().x()*100+
//                 iterator_fitxes.value()->coordenades().y();
//             //Contam el nombre de fitxes per a cada conjunt
//             llistaDeNumFitxesPerConjunt.replace(iterator_fitxes.value()->conjuntAillament()-1,
//                    llistaDeNumFitxesPerConjunt.value(iterator_fitxes.value()->conjuntAillament()-1)+1);
//             for(int j=0 ; j<iterator_fitxes.value()->movimentsPossibles.count();++j){
//                 int index_fitxaBotada=iterator_fitxes.value()->movimentsPossibles.at(j).x();
//                 int index_fitxaFinal=iterator_fitxes.value()->movimentsPossibles.at(j).y();
//                 if ( (m_fitxes.value(index_fitxaBotada)->estat()==p_estatFitxaMitjana) &&
//                      (m_fitxes.value(index_fitxaFinal)->estat()==0) ){

//                        itemsArbre4.append(QString("%1 %2 %3").arg(
//                            index_fitxaInicial).arg(
//                            index_fitxaBotada).arg(
//                            index_fitxaFinal));

//                        llistaMovimentsPerConjunt.replace(iterator_fitxes.value()->conjuntAillament()-1,
//                               llistaMovimentsPerConjunt.value(iterator_fitxes.value()->conjuntAillament()-1)+1);

//                     if((p_movimentsUndoStack->index()>variablesSolucio.minimIndexDeMoviments) &&
//                        ( (numeroDeConjuntsActual>=variablesSolucio.minimNumeroDeConjuntsActual) ||
//                          (numeroDeFitxesAilladesActual>=variablesSolucio.minimNumeroDeFitxesAilladesActual) )
//                          ){

//                     ferMoviment(index_fitxaInicial,index_fitxaBotada,index_fitxaFinal);

//                     int numeroDeConjunts=solucionaJocActual_FitxesAillades_PerConjunts();
//                     int numeroDeFitxesAillades=solucionaJocActual_numeroFitxesAillades();

//                    if(solucionaJocActual_conjuntsDispersos(numeroDeConjunts)<2){

//                     if(numeroDeConjunts<=numeroDeConjuntsActual &&
//                        numeroDeFitxesAillades<=numeroDeFitxesAilladesActual){
//                        redueixConjuntsAillades.append(QString("%1 %2 %3").arg(
//                                           index_fitxaInicial).arg(
//                                           index_fitxaBotada).arg(
//                                           index_fitxaFinal));
//                       }
//                     else if (numeroDeFitxesAillades<=numeroDeFitxesAilladesActual ||
//                              numeroDeConjunts<=numeroDeConjuntsActual){
//                       redueixAillades.append(QString("%1 %2 %3").arg(
//                                           index_fitxaInicial).arg(
//                                           index_fitxaBotada).arg(
//                                           index_fitxaFinal));
//                      }
//                     else itemsArbre4.append(QString("%1 %2 %3").arg(
//                                                 index_fitxaInicial).arg(
//                                                 index_fitxaBotada).arg(
//                                                 index_fitxaFinal));
//                    }

//                   if(jocResolt !=2){
//                     p_movimentsUndoStack->undo();
//                   }
//                   else {
//                       qDebug("Joc resolt tauler!!*****");
//                       itemsArbre4.clear();
//                       return itemsArbre4;
//                      }

//                    }//if amb control de conjunts
//                   }//if condicions fitxes (mitjana i final)
//                } //for
//             }//if inicial estat==1
//           }//while


//int conjuntSenseMoviments=0;
//for(int m=0 ; m<llistaMovimentsPerConjunt.count();++m){
//// qDebug("llistaDeNumFitxesPerConjunt.value(m) %d %d",m,llistaDeNumFitxesPerConjunt.value(m));
//// qDebug("llistaMovimentsPerConjunt.value(m) %d %d",m,llistaMovimentsPerConjunt.value(m));
// if(llistaMovimentsPerConjunt.value(m)==0 &&
//    llistaDeNumFitxesPerConjunt.value(m)>1
//         ){
//  conjuntSenseMoviments++;
// }
//}

////itemsArbre4.clear();
//if(conjuntSenseMoviments>variablesSolucio.minimNumeroDeFitxesAilladesActual ||
//   solucionaJocActual_conjuntsDispersos(llistaMovimentsPerConjunt.count())>1
//        ){
////  qDebug("conjuntSenseMoviments %d",conjuntSenseMoviments);
// // pausa(2000);
//  itemsArbre4.clear();
//  return itemsArbre4;
//}

//if(redueixConjuntsAillades.count()>0){
//    return redueixConjuntsAillades;
// }
//else if(redueixAillades.count()>0){
//  return redueixAillades;
//}
//else {
//   if (p_movimentsUndoStack->index()>variablesSolucio.minimIndexMovimentsPerFiltrarMovimentsSeleccionats){
////      if (itemsArbre4.count()>4){
//        return solucionaJocActual_CalculaNumeroMovimentsPosteriors(itemsArbre4);}
//    else return itemsArbre4;
// }
//}


/* Selecció de moviments. Ho fa servir el procediment de cerca de solució
 * solucionaJocActual_RecercaIterativa_Exhaustiva1
 * (jocs en diagonal, amb final marcat i amb més de 40 fitxes*/
QStringList Tauler::solucionaJocActual_CarregaMovimentsArbre4(int numeroInicialFitxesJoc){

     QMapIterator <int, Fitxa*> iterator_fitxes(m_fitxes);
     QStringList itemsArbre4,itemsArbre0/*,
             itemsArbreFitxesMarcadesFinalJoc*/; //tots els moviments possibles

     solucionaJocActual_CalculaMovimentsPosibles();
     int numeroDeFitxesAilladesActual=solucionaJocActual_numeroFitxesAillades();
     int numeroDeConjuntsActual=solucionaJocActual_FitxesAillades_PerConjunts();
     int numeroConjuntsDispersos=solucionaJocActual_conjuntsDispersos(numeroDeConjuntsActual);
     int numeroDeConjuntsDispersosActuals=solucionaJocActual_conjuntsDispersos(numeroDeConjuntsActual);


     // va bé per al clàssic angles
     while ( (iterator_fitxes.hasNext() && numeroDeFitxesAilladesActual<=0
              && numeroDeConjuntsActual<=3
              && numeroConjuntsDispersos<=0
              && numeroDeConjuntsDispersosActuals<=0)
             ||
             (iterator_fitxes.hasNext()
              && p_movimentsUndoStack->index()> numeroInicialFitxesJoc-8)
             ||
             (iterator_fitxes.hasNext() && numeroInicialFitxesJoc<15)
             ) {
             iterator_fitxes.next();
             if ( iterator_fitxes.value()->estat()==1){
             int index_fitxaInicial=
                 iterator_fitxes.value()->coordenades().x()*100+
                 iterator_fitxes.value()->coordenades().y();

             for(int j=0 ; j<iterator_fitxes.value()->movimentsPossibles.count();++j){
                 int index_fitxaBotada=iterator_fitxes.value()->movimentsPossibles.at(j).x();
                 int index_fitxaFinal=iterator_fitxes.value()->movimentsPossibles.at(j).y();
                 if ( (m_fitxes.value(index_fitxaBotada)->estat()==p_estatFitxaMitjana) &&
                      (m_fitxes.value(index_fitxaFinal)->estat()==0) ){

                        itemsArbre4.append(QString("%1 %2 %3").arg(
                            index_fitxaInicial).arg(
                            index_fitxaBotada).arg(
                            index_fitxaFinal));

                   }//if condicions fitxes (mitjana i final)
                } //for
             }//if inicial estat==1
           }//while

if(jocAmbFinalMarcat()>0 && !ocuparFitxesFinalJoc()){
  return itemsArbre0;
}

else if (itemsArbre4.count()>3 && p_movimentsUndoStack->index()<numeroInicialFitxesJoc-5){
    return solucionaJocActual_CalculaNumeroMovimentsPosteriors(itemsArbre4);}
else return itemsArbre4;
}

//No es fa servir
//QStringList Tauler::solucionaJocActual_CarregaMovimentsArbre2
//                     (int numeroInicialFitxesJoc,
//                      variables_recerca_solucio variablesSolucio){

//     QMapIterator <int, Fitxa*> iterator_fitxes(m_fitxes);
//     QStringList itemsArbre1,itemsArbre4; //tots els moviments possibles
//     QStringList redueixConjuntsAillades, redueixAillades, redueixConjunts,
//             mantenConjunts,mantenAillades;

//     solucionaJocActual_CalculaMovimentsPosibles();
////     int numeroDeFitxesDelJoc=numeroDeMovimentsJoc();
//     int numeroDeConjuntsActual=solucionaJocActual_FitxesAillades_PerConjunts();
//     int numeroDeFitxesAilladesActual=solucionaJocActual_numeroFitxesAillades();
//     int numeroDeConjuntsDispersosActuals=solucionaJocActual_conjuntsDispersos(numeroDeConjuntsActual);
////     if(p_movimentsUndoStack->index()>35){
////       qDebug("moviment %d  conjunts %d  aillades %d",
////              p_movimentsUndoStack->index(),numeroDeConjuntsActual,numeroDeFitxesAilladesActual);
////     }

//     while ( (iterator_fitxes.hasNext()
//              && numeroInicialFitxesJoc>34
//              && numeroDeConjuntsActual<=2
//              && numeroDeFitxesAilladesActual<=2
//              && numeroDeConjuntsDispersosActuals<3)
////             //clàssic
//             || (iterator_fitxes.hasNext()
//             && numeroInicialFitxesJoc>=31
//             && numeroDeConjuntsActual<=1
//             && numeroDeFitxesAilladesActual<=1
//             && numeroDeConjuntsDispersosActuals<2)
//              ||
//             (iterator_fitxes.hasNext() && numeroInicialFitxesJoc<= 15) )
//                     {
//             iterator_fitxes.next();
//             if ( iterator_fitxes.value()->estat()==1){
//             int index_fitxaInicial=
//                 iterator_fitxes.value()->coordenades().x()*100+
//                 iterator_fitxes.value()->coordenades().y();
//             for(int j=0 ; j<iterator_fitxes.value()->movimentsPossibles.count();++j){
//                 int index_fitxaBotada=iterator_fitxes.value()->movimentsPossibles.at(j).x();
//                 int index_fitxaFinal=iterator_fitxes.value()->movimentsPossibles.at(j).y();
//                 if ( (m_fitxes.value(index_fitxaBotada)->estat()==p_estatFitxaMitjana) &&
//                     (m_fitxes.value(index_fitxaFinal)->estat()==0) ){
//                     itemsArbre4.append(QString("%1 %2 %3").arg(
//                                           index_fitxaInicial).arg(
//                                           index_fitxaBotada).arg(
//                                           index_fitxaFinal));

////                     if((p_movimentsUndoStack->index()>variablesSolucio.minimIndexDeMoviments) &&
////                         ( (numeroDeConjuntsActual>=variablesSolucio.minimNumeroDeConjuntsActual) ||
////                          (numeroDeFitxesAilladesActual>=variablesSolucio.minimNumeroDeFitxesAilladesActual) ) ){

////                     ferMoviment(index_fitxaInicial,index_fitxaBotada,index_fitxaFinal);
////                     int numeroDeConjunts=solucionaJocActual_FitxesAillades_PerConjunts();
////                     int numeroDeFitxesAillades=solucionaJocActual_numeroFitxesAillades();
////                     int numeroDeConjuntsDispersos=solucionaJocActual_conjuntsDispersos(numeroDeConjunts);

////                     if(numeroDeConjunts<=numeroDeConjuntsActual &&
////                        numeroDeFitxesAillades<=numeroDeFitxesAilladesActual
////                        && numeroDeConjuntsDispersos>3){
////                        redueixConjuntsAillades.append(QString("%1 %2 %3").arg(
////                                           index_fitxaInicial).arg(
////                                           index_fitxaBotada).arg(
////                                           index_fitxaFinal));
////                       }
////                     else if ((numeroDeFitxesAillades<=numeroDeFitxesAilladesActual ||
////                              numeroDeConjunts<=numeroDeConjuntsActual)
////                              && numeroDeConjuntsDispersos>3){
////                       redueixAillades.append(QString("%1 %2 %3").arg(
////                                           index_fitxaInicial).arg(
////                                           index_fitxaBotada).arg(
////                                           index_fitxaFinal));
////                      }
////                   if(jocResolt !=2){
////                     p_movimentsUndoStack->undo();
////                   }
////                   else {
////                       qDebug("Joc resolt tauler!!*****");
////                       itemsArbre4.clear();
////                       return itemsArbre4;
////                      }
////                    }//if amb control de conjunts
//                   }//if condicions fitxes (mitjana i final)
//                } //for
//             }//if inicial estat==1
//           }//while

////return redueixConjuntsAillades;
////if(redueixConjuntsAillades.count()>0){
////   return redueixConjuntsAillades;
////}
////else if(redueixAillades.count()>0){
////    return redueixAillades;
////}
////else if(redueixConjunts.count()>0){
////    return redueixConjunts;
////}
////else if(mantenAillades.count()>0){
////    return mantenAillades;
////}
////else if(mantenConjunts.count()>0){
////    return mantenConjunts;
////}
////else {
////    if (p_movimentsUndoStack->index()<variablesSolucio.minimIndexMovimentsPerFiltrarMovimentsSeleccionats){
////        return solucionaJocActual_CalculaNumeroMovimentsPosteriors(itemsArbre4);}
////    else return itemsArbre4;
//// }
////qDebug("aqyuu");
////if (itemsArbre1.count()>0){
////   if (p_movimentsUndoStack->index()>variablesSolucio.minimIndexMovimentsPerFiltrarMovimentsSeleccionats){
////      return solucionaJocActual_CalculaNumeroMovimentsPosteriors(itemsArbre1);}
////   else return itemsArbre1;
////}
////else {
////    if (p_movimentsUndoStack->index()>variablesSolucio.minimIndexMovimentsPerFiltrarMovimentsSeleccionats){
////   if (p_movimentsUndoStack->index()<15 && numeroDeFitxesDelJoc>15){
////   if (p_movimentsUndoStack->index()<20 && numeroDeFitxesDelJoc>15){ //0.6
//   if (p_movimentsUndoStack->index()<numeroInicialFitxesJoc*0.9 && numeroInicialFitxesJoc>15){
////     if(itemsArbre4.count()>5){
//        return solucionaJocActual_CalculaNumeroMovimentsPosteriors(itemsArbre4);}
//    else return itemsArbre4;
//// }
//}

/* Càlcul de moviments de jocs amb poques fitxes, nombre mitjà de fitxes
 * i el cas del solitari en estrella*/
void Tauler::solucionaJocActual_CalculaArbreJoc(QProgressDialog *progres,
                                                int numFitxesInicialsJoc,
                                                int mantenirMoviment){
    jocsRealitzats=0;
    jocsSenseSolucio=0;
    QStringList movimentsActuals=solucionaJocActual_CalculaArbreJoc_CalculaMoviments();
//    if (movimentsActuals.count()==0){
//     solucionaJocActual_CalculaArbreJoc_escriuMoviments();
//     p_movimentsUndoStack->undo();
//    }
    int movimentAtzar=0;
    if(movimentsActuals.count()>0){
       movimentAtzar=qrand() % movimentsActuals.count();}

    for(int i=0;i<movimentsActuals.count();i++){
        if(jocResolt !=2 && p_movimentsUndoStack->index()>mantenirMoviment){
           p_movimentsUndoStack->undo();}
        if(jocResolt !=2){
        QStringList movimentsItem=QStringList(movimentsActuals.value(
                                (movimentAtzar+i) % movimentsActuals.count()).split(" "));
        ferMoviment2(movimentsItem.value(0).toInt()
                                ,movimentsItem.value(1).toInt(),movimentsItem.value(2).toInt());
        //Iteram
        solucionaJocActual_CalculaArbreJocIteracio(progres,numFitxesInicialsJoc,mantenirMoviment);
        continuar=true;
        }
//        qDebug("i %d",i);
//        qDebug("jocsRealitzats %d",jocsRealitzats);
    }
//    qDebug("jocsRealitzats %d",jocsRealitzats);
//    qDebug("jocsSenseSolucioB %d",jocsSenseSolucio);
}

/* Ho fa servir el procd. anterior*/
void Tauler::solucionaJocActual_CalculaArbreJocIteracio(QProgressDialog *progres,
                                                        int numFitxesInicialsJoc,
                                                        int mantenirMoviment){
   int maximNombreDeProves=100;
   //Solitari Estrella
      if (numFitxesInicialsJoc==24){
         maximNombreDeProves=100;
      }
   if (numFitxesInicialsJoc>40){
      maximNombreDeProves=500;
   }
   if(continuar){

   if(progres->wasCanceled()){
     progres->cancel();
//     qDebug("jocsRealitzats %d",jocsRealitzats);
//     qDebug("jocsSenseSolucio %d",jocsSenseSolucio);
     return;
   }
  QStringList movimentsActuals=solucionaJocActual_CalculaArbreJoc_CalculaMoviments();

  int movimentAtzar=0;
  if(movimentsActuals.count()>0){
     movimentAtzar=qrand() % movimentsActuals.count();}
  else {
      jocsSenseSolucio++;
      jocsRealitzats++;
  }

  for(int i=0;i<movimentsActuals.count();i++){

      if(progres->wasCanceled()){
        progres->cancel();
//        qDebug("jocsRealitzatsB %d",jocsRealitzats);
//        qDebug("jocsSenseSolucioB %d",jocsSenseSolucio);
        break;
      }
      QStringList movimentsItem=QStringList(movimentsActuals.value(
                          (movimentAtzar+i) % movimentsActuals.count()).split(" "));
      ferMoviment2(movimentsItem.value(0).toInt()
                              ,movimentsItem.value(1).toInt(),movimentsItem.value(2).toInt());

      if(p_movimentsUndoStack->count()<progres->maximum()){
      progres->setValue(p_movimentsUndoStack->count());
//      progres->setLabelText(p_frmPrinci->missatgeNumeroMoviments(numFitxesInicialsJoc-1));
      progres->setLabelText(p_frmPrinci->missatgeNumeroMoviments(progres->maximum()));
      qApp->processEvents();}

      int numeroDeConjuntsActual=0;
      int numeroDeFitxesAilladesActual=0;
      int numeroDeConjuntsDispersosActuals=0;
      if(p_movimentsUndoStack->index()>numeroDeMovimentsJoc()*0.1 /*&&
         p_movimentsUndoStack->index()<numeroDeMovimentsJoc()*0.95*/     ){
        numeroDeConjuntsActual=solucionaJocActual_FitxesAillades_PerConjunts();
        numeroDeFitxesAilladesActual=solucionaJocActual_numeroFitxesAillades();
        numeroDeConjuntsDispersosActuals=solucionaJocActual_conjuntsDispersos(numeroDeConjuntsActual);
      }
//      if(numeroDeFitxesAilladesActual>2){
//          qDebug("numeroDeFitxesAilladesActual %d", numeroDeFitxesAilladesActual);
//      }
//      if(numeroDeConjuntsDispersosActuals>1){
////          qDebug("numeroDeConjuntsDispersosActuals %d", numeroDeConjuntsDispersosActuals);
//      }
//      if(numeroDeConjuntsActual>2){
//          qDebug("numeroDeConjuntsActual %d", numeroDeConjuntsActual);
//      }
      //Iteram
            //Wiegleb
      if((numFitxesInicialsJoc>=40 && numeroDeFitxesAilladesActual<=1 && numeroDeConjuntsDispersosActuals<2
          && numeroDeConjuntsActual<=2)
          ||
          // Europeu
          (numFitxesInicialsJoc>=35 && numeroDeFitxesAilladesActual<=1 && numeroDeConjuntsDispersosActuals<1
          && numeroDeConjuntsActual<=2)
          ||
         //Clàssic i
//         (numeroDeMovimentsJoc()>=17 && numeroDeFitxesAilladesActual<=1 && numeroDeConjuntsDispersosActuals<2
//         && numeroDeConjuntsActual<=2)
              (numFitxesInicialsJoc>=17 && numeroDeFitxesAilladesActual<=0 && numeroDeConjuntsDispersosActuals<2
              && numeroDeConjuntsActual<=3)
         ||
         (numFitxesInicialsJoc<17)
          //Solitari estrella
         || (p_tipusMoviment==3 && numeroDeFitxesAilladesActual<=2 /*&& numeroDeConjuntsDispersosActuals<=11*/
                  && numeroDeConjuntsActual<=4)
         ){//3?
         solucionaJocActual_CalculaArbreJocIteracio(progres,numFitxesInicialsJoc,mantenirMoviment);
        }
      else {
          p_movimentsUndoStack->undo();
          jocsRealitzats++;
//          p_frmPrinci->mostraMissatge(jocsRealitzats);
          progres->setValue(p_movimentsUndoStack->count());
//          progres->setLabelText(p_frmPrinci->missatgeNumeroMoviments(numFitxesInicialsJoc-1));
          progres->setLabelText(p_frmPrinci->missatgeNumeroMoviments(progres->maximum()));
          qApp->processEvents();}
//  qDebug("i del segon bucle %d",i);
  }

  if (movimentsActuals.count()==0 && jocResolt==2){
      progres->cancel();
//      qDebug("jocsRealitzats %d",jocsRealitzats);
//      qDebug("jocsSenseSolucio %d",jocsSenseSolucio);
  }  

  if(jocResolt !=2 && p_movimentsUndoStack->index()>mantenirMoviment){
     p_movimentsUndoStack->undo();}

  if((jocsRealitzats / maximNombreDeProves)==trunc(jocsRealitzats/maximNombreDeProves) &&
     (jocsRealitzats / maximNombreDeProves>1)){
    //qDebug("divisio %f",jocsRealitzats / maximNombreDeProves);
    continuar=false;
    }
   }//continuar inicial
}

/* Ho fa servir solucionaJocActual_CalculaArbreJocIteracio() per calcular els moviments
 * de jocs amb poques fitxes o amb un nombre mitjancer de fitxes*/
QStringList Tauler::solucionaJocActual_CalculaArbreJoc_CalculaMoviments(){
    solucionaJocActual_CalculaMovimentsPosibles();
    QStringList llistaMoviments, llistaMoviments0;
    QMapIterator <int, Fitxa*> iterator_fitxes(m_fitxes);
    while  (iterator_fitxes.hasNext()){
        iterator_fitxes.next();
        if ( iterator_fitxes.value()->estat()==1){
        int index_fitxaInicial=
            iterator_fitxes.value()->coordenades().x()*100+
            iterator_fitxes.value()->coordenades().y();
        for(int j=0 ; j<iterator_fitxes.value()->movimentsPossibles.count();++j){
            int index_fitxaBotada=iterator_fitxes.value()->movimentsPossibles.at(j).x();
            int index_fitxaFinal=iterator_fitxes.value()->movimentsPossibles.at(j).y();
            if ( (m_fitxes.value(index_fitxaBotada)->estat()==p_estatFitxaMitjana) &&
                (m_fitxes.value(index_fitxaFinal)->estat()==0) ){
                llistaMoviments.append(QString("%1 %2 %3").arg(
                                      index_fitxaInicial).arg(
                                      index_fitxaBotada).arg(
                                      index_fitxaFinal));
            }//if
        }//for
       }//if estat
    }//while

//return llistaMoviments;
if(!ocuparFitxesFinalJoc()){
  return llistaMoviments0;
}
else if(numeroDeMovimentsJoc()<16){
  return llistaMoviments;
}
else if(/*p_movimentsUndoStack->index()< numeroDeMovimentsJoc()*1.0 &&*/
//   numeroDeMovimentsJoc()>=17  ){//    return solucionaJocActual_CalculaNumeroMovimentsPosteriors(llistaMoviments);
    numeroDeMovimentsJoc()>=20 && p_movimentsUndoStack->index()> numeroDeMovimentsJoc()*0.2  //24
    /*&& p_movimentsUndoStack->index()<numeroDeMovimentsJoc()*0.8*/){
   return solucionaJocActual_CalculaNumeroMovimentsPosteriors(llistaMoviments);

 }
else if (llistaMoviments.count()>8){
//  return solucionaJocActual_RedueixLlistaDeMoviments(llistaMoviments);
  return solucionaJocActual_CalculaNumeroMovimentsPosteriors(llistaMoviments);
}
else return llistaMoviments;
}

/* Procediment de recerca exhaustiva de solucions sense filtres de cap tipus.
 * Quan troba una solució, escriu en un arxiu .arbre el nombre de jocs, el nombre
 * de solucions trobades i el temps.
 * Ho fa servir el procediment de cerca de solució per foça bruta
 * frmPrincipal::solucionaJoc_ForcaBruta()
 * És possible escriure els resultats en un arxiu de text
 */
void Tauler::solucionaJocActual_CalculaArbreJocComplet_Inici(QProgressDialog *progres,
                                                       int numFitxesInicialsJoc){

QStringList movimentsActuals=solucionaJocActual_CalculaArbreJocComplet_Inici_CalculaMoviments();

if(!progres->wasCanceled()){
int movimentAtzar=0;
//if(movimentsActuals.count()>0){
//  movimentAtzar= qrand() % movimentsActuals.count();}

for(int i=0;i<movimentsActuals.count();i++){
    if(jocResolt !=2 && !progres->wasCanceled()){
    QStringList movimentsItem=QStringList(movimentsActuals.value(
                                         (movimentAtzar+i) % movimentsActuals.count()).split(" "));
    ferMoviment2(movimentsItem.value(0).toInt()
                            ,movimentsItem.value(1).toInt(),movimentsItem.value(2).toInt());
    if(p_movimentsUndoStack->index()<progres->maximum()){
    progres->setValue(p_movimentsUndoStack->index());
    progres->setLabelText(p_frmPrinci->missatgeNumeroMoviments(progres->maximum()));
    }
    qApp->processEvents();

    int numeroDeConjuntsActual=solucionaJocActual_FitxesAillades_PerConjunts();
    int numeroDeFitxesAilladesActual=solucionaJocActual_numeroFitxesAillades();
    int numeroDeConjuntsDispersosActuals=solucionaJocActual_conjuntsDispersos(numeroDeConjuntsActual);
    int distanciaMaximaDelsConjunts=solucionaJocActual_conjuntsAillats(numeroDeConjuntsActual);

//        if(distanciaMaximaDelsConjunts<4 && numeroDeFitxesAilladesActual<4){
//        if(numeroDeConjuntsActual<3 /*|| distanciaMaximaDelsConjunts<3*/){
        if(numeroDeConjuntsDispersosActuals<2 && numeroDeFitxesAilladesActual<3
           && distanciaMaximaDelsConjunts<3){
            //Iteram
            solucionaJocActual_CalculaArbreJocComplet_Inici(progres,numFitxesInicialsJoc);
        }
//        else {
//            qDebug("desfem moviment");
//            p_movimentsUndoStack->undo();
//        }
    }
    if (jocResolt ==2){
      qApp->processEvents();
//      p_frmPrinci->desaSolucions1();
      //Això no es fa per evitar la pèrdua de temps de carregar continuament les
      //solucions.
//      p_frmPrinci->veureSolucionsSlot();
      solucionaJocActual_CalculaArbreJoc_escriuMoviments();
      qApp->processEvents();
//      QString missatge=QString("%1 %2 %3").arg(jocsRealitzats).
//              arg(jocsAmbSolucio).
//              arg(p_frmPrinci->rellotgeLabel->retornaTemps(1))
//              ;
//      qCritical("%s",qPrintable(missatge));
      p_frmPrinci->mostraMissatge(100);
      jocResolt=0;
      p_movimentsUndoStack->undo();
//      break;
    }
    else  {
        //no anotam els casos en què no es troba solució
        solucionaJocActual_CalculaArbreJoc_escriuMoviments();
        p_movimentsUndoStack->undo();
        /*jocsRealitzats++;*/}
   }//for
 }///if inicial
}

//void Tauler::solucionaJocActual_CalculaArbreJocComplet_Inici(QProgressDialog *progres,
//                                                       int numFitxesInicialsJoc){

//QStringList movimentsActuals=solucionaJocActual_CalculaArbreJocComplet_Inici_CalculaMoviments();

//if(!progres->wasCanceled()){
//int movimentAtzar=0;
////if(movimentsActuals.count()>0){
////  movimentAtzar= qrand() % movimentsActuals.count();}

//for(int i=0;i<movimentsActuals.count();i++){
//    if(jocResolt !=2 && !progres->wasCanceled()){
//    QStringList movimentsItem=QStringList(movimentsActuals.value(
//                                         (movimentAtzar+i) % movimentsActuals.count()).split(" "));
//    ferMoviment2(movimentsItem.value(0).toInt()
//                            ,movimentsItem.value(1).toInt(),movimentsItem.value(2).toInt());
//    progres->setValue(p_movimentsUndoStack->count());
//    progres->setLabelText(p_frmPrinci->missatgeNumeroMoviments(progres->maximum()));
//    qApp->processEvents();
//    //Iteram
//    solucionaJocActual_CalculaArbreJocComplet_Inici(progres,numFitxesInicialsJoc);
//    }
//    if (jocResolt ==2){
//      qApp->processEvents();
////      p_frmPrinci->desaSolucions1();
//      //Això no es fa per evitar la pèrdua de temps de carregar continuament les
//      //solucions.
////      p_frmPrinci->veureSolucionsSlot();
//      solucionaJocActual_CalculaArbreJoc_escriuMoviments();
//      qApp->processEvents();
////      QString missatge=QString("%1 %2 %3").arg(jocsRealitzats).
////              arg(jocsAmbSolucio).
////              arg(p_frmPrinci->rellotgeLabel->retornaTemps(1))
////              ;
////      qCritical("%s",qPrintable(missatge));
//      p_frmPrinci->mostraMissatge(100);
//      jocResolt=0;
//      p_movimentsUndoStack->undo();
////      break;
//    }
//    else  {
//        //no anotam els casos en què no es troba solució
//        solucionaJocActual_CalculaArbreJoc_escriuMoviments();
//        p_movimentsUndoStack->undo();
//        /*jocsRealitzats++;*/}
//   }//for
// }///if inicial
//}

/* Càlcul dels moviments possibles sense restriccions. Ho fa servir
 * solucionaJocActual_CalculaArbreJocComplet_Inici
 * el mètode de força bruta
 */
QStringList Tauler::solucionaJocActual_CalculaArbreJocComplet_Inici_CalculaMoviments(){
    solucionaJocActual_CalculaMovimentsPosibles();
    QStringList llistaMoviments;
    QMapIterator <int, Fitxa*> iterator_fitxes(m_fitxes);
    while  (iterator_fitxes.hasNext()){
        iterator_fitxes.next();
        if ( iterator_fitxes.value()->estat()==1){
        int index_fitxaInicial=
            iterator_fitxes.value()->coordenades().x()*100+
            iterator_fitxes.value()->coordenades().y();
        for(int j=0 ; j<iterator_fitxes.value()->movimentsPossibles.count();++j){
            int index_fitxaBotada=iterator_fitxes.value()->movimentsPossibles.at(j).x();
            int index_fitxaFinal=iterator_fitxes.value()->movimentsPossibles.at(j).y();
            if ( (m_fitxes.value(index_fitxaBotada)->estat()==p_estatFitxaMitjana) &&
                (m_fitxes.value(index_fitxaFinal)->estat()==0) ){
                llistaMoviments.append(QString("%1 %2 %3").arg(
                                      index_fitxaInicial).arg(
                                      index_fitxaBotada).arg(
                                      index_fitxaFinal));
            }//if
        }//for
       }//if estat
    }//while
//return solucionaJocActual_CalculaNumeroMovimentsPosteriors(llistaMoviments);
return llistaMoviments;
}

/* Selecció de moviments per la majoria de jocs Clàssic i Europeu*/
void Tauler::solucionaJocActual_CalculaArbreJocComplet_Selectiu(QProgressDialog *progres,
                                                       int numFitxesInicialsJoc,
                                                       int mantenirMoviment){

      //qDebug("divisio %f",jocsRealitzats / maximNombreDeProves);

    if (jocResolt !=2 && !progres->wasCanceled() && continuar){

    QStringList movimentsActuals=solucionaJocActual_CalculaArbreJocComplet_CalculaMoviments();
//    qDebug("movimentsActuals.count() %d", movimentsActuals.count());
    if(!progres->wasCanceled() && jocResolt !=2){
    int movimentAtzar=0;
    if(movimentsActuals.count()>0 && (numFitxesInicialsJoc<=40
      || (numFitxesInicialsJoc> 40 && p_movimentsUndoStack->index()<35) )
      /*&& p_movimentsUndoStack->index()>numFitxesInicialsJoc*0.3*/){
      movimentAtzar=qrand() % movimentsActuals.count();
    }

    for(int i=0;i<movimentsActuals.count();i++){
        QStringList movimentsItem=QStringList(movimentsActuals.value(
                          (movimentAtzar+i) % movimentsActuals.count()).split(" "));
        ferMoviment2(movimentsItem.value(0).toInt()
                                ,movimentsItem.value(1).toInt(),movimentsItem.value(2).toInt());
        if(jocResolt !=2 && !progres->wasCanceled() && continuar){

        /* Això és per evitar problemes amb els jocs amb final marcat*/
        if(p_movimentsUndoStack->index()<progres->maximum()){
        progres->setValue(p_movimentsUndoStack->index());
        progres->setLabelText(p_frmPrinci->missatgeNumeroMoviments(progres->maximum()));
        }
        qApp->processEvents();}
        int numeroDeConjuntsActual=0;
        int numeroDeFitxesAilladesActual=0;
//        int numeroDeConjuntsDispersosActuals=0;
        int distanciaMaximaDelsConjunts=0;

//        if(jocAmbFinalMarcat()==0){
          numeroDeConjuntsActual=solucionaJocActual_FitxesAillades_PerConjunts();
          numeroDeFitxesAilladesActual=solucionaJocActual_numeroFitxesAillades();
//          numeroDeConjuntsDispersosActuals=solucionaJocActual_conjuntsDispersos(numeroDeConjuntsActual);
//          if(numFitxesInicialsJoc< 40){
          distanciaMaximaDelsConjunts=solucionaJocActual_conjuntsAillats(numeroDeConjuntsActual);
//          }
          //Classic Asimètric
//          numeroDeConjuntsActual<=1 && numeroDeFitxesAilladesActual<=0 && numeroDeConjuntsDispersosActuals<=1

        if( (numFitxesInicialsJoc<=21 )
            ||
             (numFitxesInicialsJoc<=40
             && numeroDeConjuntsActual<=2 && numeroDeFitxesAilladesActual<=0
              && distanciaMaximaDelsConjunts<=3)
            ||
           (numFitxesInicialsJoc> 100 && distanciaMaximaDelsConjunts<=3
            && numeroDeFitxesAilladesActual==0
            /*&& numeroDeConjuntsDispersosActuals ==0*/ && numeroDeConjuntsActual<=3
            )
            ||
            (jocAmbFinalMarcat()>0 && ocuparFitxesFinalJoc() )

           ){
           //Iteram
           solucionaJocActual_CalculaArbreJocComplet_Selectiu(progres,numFitxesInicialsJoc,mantenirMoviment);
         }
        if(!progres->wasCanceled() && jocResolt !=2
            && p_movimentsUndoStack->index()>mantenirMoviment){
           p_movimentsUndoStack->undo();
           jocsRealitzats++;
        if(jocsRealitzats>=maximNombreDeProves){
          continuar=false;
//          qDebug("jocsRealitzats for %d", jocsRealitzats);
          jocsRealitzats=0;
         }
        }
       }//for

     }///if inicial
  }
 else if (continuar){
     progres->cancel();
     progres->setVisible(false);
    } 
if(jocsRealitzats>=maximNombreDeProves){
  continuar=false;
  jocsRealitzats=0;
}
}

/* Comprova si el joc te el final marcat*/
int Tauler::jocAmbFinalMarcat(){
 int finalMarcat=0;
 QMapIterator <int, Fitxa*> iterator_fitxes(m_fitxes);
 while  (iterator_fitxes.hasNext()){
     iterator_fitxes.next();
     if ( iterator_fitxes.value()->fitxaFinalJoc()){
      finalMarcat++;
//      qDebug("final marcat!");
//      break;
    }//if estat
 }//while
 return finalMarcat;
}

/* Dels moviments passats, selecciona els que tenen una fitxa marcada com a final
 * de joc com a final de moviment*/
QStringList Tauler::solucionaJocActual_seleccionarMovimentsAmbFitxesFinalsDeJoc(QStringList moviments){
    QStringList movimentsAmbFitxaFinalJoc1,movimentsAmbFitxaFinalJoc2,
            movimentsAmbFitxaFinalJoc3,movimentsAmbFitxaFinalJoc0;/*movimentsAmbFitxaFinalJoc4*/
    for(int v=0 ; v<moviments.count();++v){
        QStringList fitxes=QStringList(moviments.value(v).split(" "));
        //Cap de les fitxes és final de joc
        if(!m_fitxes.value(fitxes.value(0).toInt())->fitxaFinalJoc() &&
           !m_fitxes.value(fitxes.value(1).toInt())->fitxaFinalJoc()){
           movimentsAmbFitxaFinalJoc1.append(moviments.value(v));
        }
        //La fitxa final és final de joc (les altres dues no ho son)
        else if (!m_fitxes.value(fitxes.value(0).toInt())->fitxaFinalJoc() &&
                 !m_fitxes.value(fitxes.value(1).toInt())->fitxaFinalJoc() &&
                 m_fitxes.value(fitxes.value(2).toInt())->fitxaFinalJoc()     ){
           movimentsAmbFitxaFinalJoc1.append(moviments.value(v));
           movimentsAmbFitxaFinalJoc2.append(moviments.value(v));

        }
        //La fitxa mitjana és final de joc (les altres dues no ho son)
        else if (!m_fitxes.value(fitxes.value(0).toInt())->fitxaFinalJoc() &&
                 m_fitxes.value(fitxes.value(1).toInt())->fitxaFinalJoc() &&
                 !m_fitxes.value(fitxes.value(2).toInt())->fitxaFinalJoc()     ){
//           movimentsAmbFitxaFinalJoc1.append(moviments.value(v));
           movimentsAmbFitxaFinalJoc3.append(moviments.value(v));

        }
        //La primera final de joc, la segona no i la tercera si
        else if (m_fitxes.value(fitxes.value(0).toInt())->fitxaFinalJoc() &&
                 !m_fitxes.value(fitxes.value(1).toInt())->fitxaFinalJoc() &&
                 m_fitxes.value(fitxes.value(2).toInt())->fitxaFinalJoc()){
            movimentsAmbFitxaFinalJoc2.append(moviments.value(v));
        }
        //La primera final de joc, les altres dues no
        else if (m_fitxes.value(fitxes.value(0).toInt())->fitxaFinalJoc() &&
                 !m_fitxes.value(fitxes.value(1).toInt())->fitxaFinalJoc() &&
                 !m_fitxes.value(fitxes.value(2).toInt())->fitxaFinalJoc()){
             movimentsAmbFitxaFinalJoc1.append(moviments.value(v));
             movimentsAmbFitxaFinalJoc3.append(moviments.value(v));
         }
    }

//Si no és possible acabar el joc, no tornam moviments
if(!ocuparFitxesFinalJoc()){
 return movimentsAmbFitxaFinalJoc0;
}
//else if(movimentsAmbFitxaFinalJoc2.count()>0){
//  return movimentsAmbFitxaFinalJoc2;}
//else if(movimentsAmbFitxaFinalJoc1.count()>0){
//    return movimentsAmbFitxaFinalJoc1;}
//else if(movimentsAmbFitxaFinalJoc3.count()>0){
//    return movimentsAmbFitxaFinalJoc3;}
//else return moviments;
else if(movimentsAmbFitxaFinalJoc1.count()>0){
  return movimentsAmbFitxaFinalJoc1;
//  return solucionaJocActual_CalculaNumeroMovimentsPosteriors(movimentsAmbFitxaFinalJoc1);
  }
else if (movimentsAmbFitxaFinalJoc3.count()>0){
    return movimentsAmbFitxaFinalJoc3;
    }
else if (movimentsAmbFitxaFinalJoc2.count()>0){
    return movimentsAmbFitxaFinalJoc2;
    }
//else if (movimentsAmbFitxaFinalJoc4.count()>0){
//    return movimentsAmbFitxaFinalJoc4;
//    }
else return moviments;

}

/* Selecció de moviments. Ho fa servir Tauler::solucionaJocActual_CalculaArbreJocComplet_Selectiu*/
QStringList Tauler::solucionaJocActual_CalculaArbreJocComplet_CalculaMoviments(){
    solucionaJocActual_CalculaMovimentsPosibles();
    QStringList llistaMoviments;
    QMapIterator <int, Fitxa*> iterator_fitxes(m_fitxes);
    while  (iterator_fitxes.hasNext()){
        iterator_fitxes.next();
        if ( iterator_fitxes.value()->estat()==1){
        int index_fitxaInicial=
            iterator_fitxes.value()->coordenades().x()*100+
            iterator_fitxes.value()->coordenades().y();
        for(int j=0 ; j<iterator_fitxes.value()->movimentsPossibles.count();++j){
            int index_fitxaBotada=iterator_fitxes.value()->movimentsPossibles.at(j).x();
            int index_fitxaFinal=iterator_fitxes.value()->movimentsPossibles.at(j).y();
            if ( (m_fitxes.value(index_fitxaBotada)->estat()==p_estatFitxaMitjana) &&
                (m_fitxes.value(index_fitxaFinal)->estat()==0) ){
                llistaMoviments.append(QString("%1 %2 %3").arg(
                                      index_fitxaInicial).arg(
                                      index_fitxaBotada).arg(
                                      index_fitxaFinal));
            }//if
        }//for
       }//if estat
    }//while
if (jocAmbFinalMarcat()>10 ){
 return solucionaJocActual_seleccionarMovimentsAmbFitxesFinalsDeJoc(llistaMoviments);
    }
else if (numeroDeMovimentsJoc()<=25){
   return llistaMoviments;
}
else if(p_movimentsUndoStack->index()<numeroDeMovimentsJoc()*0.95 &&
   p_movimentsUndoStack->index()<37 ){
  return solucionaJocActual_CalculaNumeroMovimentsPosteriors(llistaMoviments);
}
else return llistaMoviments;
}

/* Escriu els moviments realitzats a un joc. Es fa servir en el procd.
 * de forçaBruta per deixar en un arxiu de text els jocs fets
 * o les solucions trobades*/

void Tauler::solucionaJocActual_CalculaArbreJoc_escriuMoviments(){
//    qDebug("jocsRealitzats %d",jocsRealitzats);
    QString solucioActual, nomesMoviments;
    solucioActual.append(p_frmPrinci->rellotgeLabel->retornaTemps(1)+" ");
    for(int j=0;j<p_movimentsUndoStack->count()-1;j++){
       solucioActual.append(QString("%1 %2 ").
                            arg(j+1).
                            arg(coordenadesAMoviment(p_movimentsUndoStack->text(j)))) ;
       nomesMoviments.append(QString("%1 %2").
                             arg(p_movimentsUndoStack->count()).
                             arg(coordenadesAMoviment(p_movimentsUndoStack->text(
                                    p_movimentsUndoStack->count()-1)))) ;
        }
     /*04/02/11  Eliminats els espais buits del final
       */
    solucioActual.append(QString("%1 %2").
                                arg(p_movimentsUndoStack->count()).
                                arg(coordenadesAMoviment(p_movimentsUndoStack->text(
                                       p_movimentsUndoStack->count()-1)))) ;

    if(jocResolt==2){
     solucioActual.prepend("*");
     qCritical("%s Solució!",qPrintable(p_frmPrinci->rellotgeLabel->retornaTemps(1)));
//     jocsAmbSolucio++;
    }
    else solucioActual.prepend(" ");
//    qCritical("%s", qPrintable(solucioActual));
    if(!darrerEscriuMoviments.contains(nomesMoviments)){
    if(jocResolt==2){jocsAmbSolucio++;}
    jocsRealitzats++;
    solucioActual.prepend(QString("%1 ").arg(jocsRealitzats));
    QString missatge=QString("%1 %2 %3 \n").arg(jocsRealitzats).
            arg(jocsAmbSolucio).
            arg(p_frmPrinci->rellotgeLabel->retornaTemps(1))
            ;
    if(jocResolt==2 || 1==1){
    QString nomArxiu=p_frmPrinci->nomArxiuJoc(5);
    QFile arxiu(nomArxiu);
      if (arxiu.open(QIODevice::Append)){
         QTextStream entrada(&arxiu);
         //per escriure totes les solucions completes
         entrada<<(QString("%1\n").arg(solucioActual)) ;
         //per escriure el nombre de moviments realitzats, solucions trobades
         //i temps emprat
//         entrada<<(missatge) ;
        }
      arxiu.close();
    }
      darrerEscriuMoviments.clear();
      darrerEscriuMoviments.append(nomesMoviments);
    }
//    //tornam enrrera el darrer moviment
//    p_movimentsUndoStack->undo();
}

 /*
   Per a cada un dels moviments passats, calcula el nombre de moviments posibles
   després de fer els moviments passats.
   Retorna la clau del Has de la fitxa amb més moviments
   */
 QStringList Tauler::solucionaJocActual_CalculaNumeroMovimentsPosteriors(QStringList movimentsPosibles){
     //qDebug("solucionaJocActual_CalculaNumeroMovimentsPosteriors");     
     QList <int> llistaNumeroMoviments;
     for(int j=0 ; j<movimentsPosibles.count();++j){
         //Primer "simulam" el moviment
         QStringList moviment=QStringList(movimentsPosibles.value(j).split(" "));
         if ( (p_tipusMoviment != 2) && (p_tipusMoviment != 5)){
             m_fitxes.value(moviment.value(0).toInt())->setEstat(0);
             m_fitxes.value(moviment.value(1).toInt())->setEstat(0);
             m_fitxes.value(moviment.value(2).toInt())->setEstat(1);

             llistaNumeroMoviments.append(solucionaJocActual_MovimentsPosiblesActuals());

             //desfem el moviment
             m_fitxes.value(moviment.value(0).toInt())->setEstat(1);
             m_fitxes.value(moviment.value(1).toInt())->setEstat(1);
             m_fitxes.value(moviment.value(2).toInt())->setEstat(0);
         }//if p_tipusMoviment != 2
         else {
             m_fitxes.value(moviment.value(0).toInt())->setEstat(0);
             m_fitxes.value(moviment.value(1).toInt())->setEstat(1);
             m_fitxes.value(moviment.value(2).toInt())->setEstat(1);
             //Ara calculam el número de moviments possibles amb aquesta situació
             llistaNumeroMoviments.append(solucionaJocActual_MovimentsPosiblesActuals());

             //desfem el moviment
             m_fitxes.value(moviment.value(0).toInt())->setEstat(1);
             m_fitxes.value(moviment.value(1).toInt())->setEstat(0);
             m_fitxes.value(moviment.value(2).toInt())->setEstat(0);
         }//else p_tipusMoviment != 2
     }//for

  return solucionaJocActual_SeleccionaNumeroMovimentsPosteriors(movimentsPosibles,llistaNumeroMoviments);

 }

/* Dels moviments possibles, selecciona els que tenen el major nombre de moviments
 * posteriors
 */
QStringList Tauler::solucionaJocActual_SeleccionaNumeroMovimentsPosteriors(QStringList llistaMovimentsMaxims,
                                                                           QList <int> llistaNumeroMoviments){
  /* Màxim nombre de moviments*/
  int maximNombreDeMoviments=0;
  for(int j=0 ; j<llistaNumeroMoviments.count();++j){
     if(llistaNumeroMoviments.value(j)>maximNombreDeMoviments){
       maximNombreDeMoviments=llistaNumeroMoviments.value(j);
      // qDebug("maximNombreDeMoviments %d", maximNombreDeMoviments);
     }

  }
  /* Seleccionam els moviments que maximitzam el nombre de moviments posteriors*/
  QStringList movimentsMaxims;
  for(int j=0 ; j<llistaNumeroMoviments.count();++j){
     if(llistaNumeroMoviments.value(j)>=maximNombreDeMoviments){
       movimentsMaxims.append(llistaMovimentsMaxims.value(j));
     }
  }
 //qDebug("movimentsMaxims.count() %d", movimentsMaxims.count());
 return movimentsMaxims;
}

/* Dels moviments possibles, selecciona aquells que redueixen el nombre de
 * conjunts de fitxes
   No es fa servir però podria ésser útil */
QStringList Tauler::solucionaJocActual_SeleccionaMovimentsRedueixenConjuntsFitxes(QStringList movimentsPosibles){
    QStringList movimentsSeleccionats;
    int numeroConjuntsActual=solucionaJocActual_FitxesAillades_PerConjunts();
    for(int j=0 ; j<movimentsPosibles.count();++j){
        //Primer "simulam" el moviment
        QStringList moviment=QStringList(movimentsPosibles.value(j).split(" "));
        if ( (p_tipusMoviment != 2) && (p_tipusMoviment != 5)){
            ferMoviment(moviment.value(0).toInt(),moviment.value(1).toInt(),moviment.value(2).toInt());
            int nouNumeroConjuntsActual=solucionaJocActual_FitxesAillades_PerConjunts();
            if(nouNumeroConjuntsActual<numeroConjuntsActual){
              numeroConjuntsActual=nouNumeroConjuntsActual;
              movimentsSeleccionats.clear();
              movimentsSeleccionats.append(movimentsPosibles.value(j));
            }
            else if (nouNumeroConjuntsActual==numeroConjuntsActual){
                numeroConjuntsActual=nouNumeroConjuntsActual;
                movimentsSeleccionats.append(movimentsPosibles.value(j));
            }
            //desfem el moviment
            if(jocResolt !=2){
            p_movimentsUndoStack->undo();}
            else {
                movimentsSeleccionats.clear();
                return movimentsSeleccionats;}
        }//if p_tipusMoviment != 2
        else {
            m_fitxes.value(moviment.value(0).toInt())->setEstat(0);
            m_fitxes.value(moviment.value(1).toInt())->setEstat(1);
            m_fitxes.value(moviment.value(2).toInt())->setEstat(1);
            if(numeroConjuntsActual<solucionaJocActual_FitxesAillades_PerConjunts()){
              movimentsSeleccionats.append(movimentsPosibles.value(j));
            }
            //desfem el moviment
            m_fitxes.value(moviment.value(0).toInt())->setEstat(1);
            m_fitxes.value(moviment.value(1).toInt())->setEstat(0);
            m_fitxes.value(moviment.value(2).toInt())->setEstat(0);
        }//else p_tipusMoviment != 2
    }//for

 return movimentsSeleccionats;
}

/* Simplement, selecciona a l'atzar alguns moviments i els retorna.
 * Es fa una reducció dels moviments que es provaran
   No es fa servir */
QStringList Tauler::solucionaJocActual_RedueixLlistaDeMoviments(QStringList movimentsPosibles){
  QStringList movimentsSeleccionats;
  int seleccio=qrand() % movimentsPosibles.count();
  for(int j=0 ; j<movimentsPosibles.count()*0.5;++j){
    movimentsSeleccionats.append(movimentsPosibles.value(
                (seleccio+j)% movimentsPosibles.count() ));
  }
  return movimentsSeleccionats;
}
 /* Comprova si s'ha arribat al final del joc. Això
  * és molt útil en les modalitats que tenen el final
  * de joc marcat
  */
bool Tauler::arribatFinalJoc(){
//QHashIterator <int, Fitxa*> iterator_fitxes(m_fitxes);
QMapIterator <int, Fitxa*> iterator_fitxes(m_fitxes);
int numeroFitxesActives=0;
while (iterator_fitxes.hasNext()) {
   iterator_fitxes.next();
   if ( iterator_fitxes.value()->estat()==1){
    ++numeroFitxesActives;}
}
bool finalDeJoc=false;
    if( (numeroFitxesActives==1) &&  (p_tipusMoviment != 2)
        && (comptadorFitxesFinalJoc==0) ){finalDeJoc=true;}
    else if ((nomesQuedenFitxesMarcadesFinalJoc())
             && (p_tipusMoviment != 2)){finalDeJoc=true;}
    else if ( (numeroFitxesActives==numeroDeMovimentsJoc()+1) )
            {finalDeJoc=true;}
 return finalDeJoc;
 }

 /*
   Per el moviment passat per paràmetre, comprova si hi ha
   moviments posteriors
   Cal tenir en compte que pot ésser el darrer moviment!!!
   19/11/12 Aquest proc. no pareix ésser eficaç. Ara no es fa
   servir

 bool Tauler::solucionaJocActual_CalculaMovimentsPosteriors(QString moviment){
         int numeroMoviments=0;
         //Primer "simulam" el moviment
         QStringList fitxes=QStringList(moviment.split(" "));
         if ( (p_tipusMoviment != 2) && (p_tipusMoviment != 5)){
         m_fitxes.value(fitxes.value(0).toInt())->setEstat(0);
         m_fitxes.value(fitxes.value(1).toInt())->setEstat(0);
         m_fitxes.value(fitxes.value(2).toInt())->setEstat(1);
         //Ara calculam el número de moviments possibles amb aquesta situació
         numeroMoviments=solucionaJocActual_MovimentsPosiblesActuals();

         //desfem el moviment
         m_fitxes.value(fitxes.value(0).toInt())->setEstat(1);
         m_fitxes.value(fitxes.value(1).toInt())->setEstat(1);
         m_fitxes.value(fitxes.value(2).toInt())->setEstat(0);}
      else {
         m_fitxes.value(fitxes.value(0).toInt())->setEstat(0);
         m_fitxes.value(fitxes.value(1).toInt())->setEstat(1);
         m_fitxes.value(fitxes.value(2).toInt())->setEstat(1);
         //Ara calculam el número de moviments possibles amb aquesta situació
         numeroMoviments=solucionaJocActual_MovimentsPosiblesActuals();
         //desfem el moviment
         m_fitxes.value(fitxes.value(0).toInt())->setEstat(1);
         m_fitxes.value(fitxes.value(1).toInt())->setEstat(0);
         m_fitxes.value(fitxes.value(2).toInt())->setEstat(0);}

   //Comprovam si estem al final del joc
  // if(arribatFinalJoc()){numeroMoviments=qMax(numeroMoviments,1);}

     return numeroMoviments>0;
 }
 */

/* Calcula el nombre de moviments reals de la configuració actual
 */
 int Tauler::solucionaJocActual_MovimentsPosiblesActuals(){ 
    QMapIterator <int, Fitxa*> iterator_fitxes(m_fitxes);
   // QHashIterator <int, Fitxa*> iterator_fitxes(m_fitxes);

    int movimentsPosibles=0;
    int numeroFitxesActives=0;
      while (iterator_fitxes.hasNext()) {
          iterator_fitxes.next();
        int index_fitxaInicial=iterator_fitxes.value()->coordenades().x()*100+iterator_fitxes.value()->coordenades().y();
        //només comprovam les fitxes actives
       if ( m_fitxes.value(index_fitxaInicial)->estat()==1){
        ++numeroFitxesActives;
        for(int j=0 ; j<iterator_fitxes.value()->movimentsPossibles.count();++j){
            int index_fitxaBotada=iterator_fitxes.value()->movimentsPossibles.at(j).x();
            int index_fitxaFinal=iterator_fitxes.value()->movimentsPossibles.at(j).y();
            if (   (m_fitxes.value(index_fitxaBotada)->estat()==p_estatFitxaMitjana) &&
                (m_fitxes.value(index_fitxaFinal)->estat()==0) ){
                ++movimentsPosibles;                
               }
           } //for
        }//if inicial estat==1
      }

if (arribatFinalJoc()){movimentsPosibles=qMax(movimentsPosibles,1);}

 return movimentsPosibles;
 }

 /*
   Comprova si hi ha alguna fitxa "aïllada"
   o sigui fitxes en estat 1 que no tenen cap moviment
   que es pugui fer efectiu o be cap moviment en què
   la fitxa mitjancera pugui passar a estat 1 desprès d'un moviment
   18/11/12 aques proc. no pareix molt eficaç
   03/09/16 Substituït pel procediement solucionaJocActual_FitxesAillades1()
   a la versió 2.1
   *
 bool Tauler::solucionaJocActual_FitxesAillades(){
     qDebug("Inici aillades*************");
     // Aquest procediment no és eficient
     // amb la modalitat inversa
     if(p_tipusMoviment==2){return false;}
     solucionaJocActual_CalculaMovimentsPosibles();
    // 22/08/16 Això no es fa servir per res
     //int nivellAillamentFitxes=0;
     int numeroFitxesAillades=0;
     //QHashIterator <int, Fitxa*> iterator_fitxes(m_fitxes);
     QMapIterator <int, Fitxa*> iterator_fitxes(m_fitxes);
     QString moviments;
    //posam totes les fitxes a no aïllades
     //while ( iterator_fitxes.hasNext() ){
     // iterator_fitxes.next();
     // iterator_fitxes.value()->setFitxaAillada(false);
     //}
     //iterator_fitxes.toFront();
     while ( iterator_fitxes.hasNext() ){
        iterator_fitxes.next();
        int index_fitxaInicial=
            iterator_fitxes.value()->coordenades().x()*100+iterator_fitxes.value()->coordenades().y();
        int index_fitxaBotada;
        int index_fitxaFinal;
        moviments.clear();
        m_fitxes.value(index_fitxaInicial)->calculaMovimentsFitxa();
        //Només poden estar "aïllades" les fitxes actives
        //(sí ho poden estar si son fitxes finals de joc!)
        //Tampoc pot estar aïllada una fitxa que és la mitjana d'un moviment posible
        if(  (m_fitxes.value(index_fitxaInicial)->estat()==1)   ||
            (!(m_fitxes.value(index_fitxaInicial)->fitxaFinalJoc()) &&
            (m_fitxes.value(index_fitxaInicial)->estat()==1) ) ){
        for(int j=0 ; j<iterator_fitxes.value()->movimentsPossibles.count();++j){
            index_fitxaBotada=iterator_fitxes.value()->movimentsPossibles.at(j).x();
            index_fitxaFinal=iterator_fitxes.value()->movimentsPossibles.at(j).y();
            //fitxa activa possibilitat de fer un moviment
            if (  (m_fitxes.value(index_fitxaInicial)->estat()==1) &&
                  (m_fitxes.value(index_fitxaBotada)->estat() ==p_estatFitxaMitjana) &&
                  (m_fitxes.value(index_fitxaFinal)->estat()==0)){
                  moviments.append("1"); //break;
              }
            else moviments.append("0");

            if (  (m_fitxes.value(index_fitxaInicial)->estat()==1) &&
                (m_fitxes.value(index_fitxaBotada)->estat() != p_estatFitxaMitjana)  ){
                //Cal mirar si la fitxa botada es el final d'un moviment possible
                if ( (!movimentPosibleFitxaFinal(index_fitxaBotada)) ){
                       moviments.append("0");
                }
                else {moviments.append("1");
                   // break;
                }
            }
            //La fitxa pot ésser la mitjana d'un moviment possible
            if (  (m_fitxes.value(index_fitxaInicial)->estat()==1)  ){
                if ( (!movimentPosibleFitxaMitjana(index_fitxaInicial)) ){
                       moviments.append("0");
                }
                else {moviments.append("1");
                   // break;
                }
            }
            if (  (m_fitxes.value(index_fitxaInicial)->estat()==1) &&
                  m_fitxes.value(index_fitxaInicial)->movimentsFitxa.count()<1){
                if ( (!movimentPosibleFitxaMitjana(index_fitxaInicial)) ){
                       moviments.append("");
                }
                else {moviments.append("0");
                   // break;
                }
            }
          }//final del for

        if ( !(moviments.contains("1"))  ){
            m_fitxes.value(index_fitxaInicial)->setFitxaAillada(true);
            if (m_fitxes.value(index_fitxaInicial)->nivellAillament()==1){
                numeroFitxesAillades=numeroFitxesAillades+1;}
        }
        else {
             m_fitxes.value(index_fitxaInicial)->setFitxaAillada(false);
        }
        qApp->processEvents();
      }//final del if inicial del while
     else {
       m_fitxes.value(index_fitxaInicial)->setFitxaAillada(false);
       }

    //if(m_fitxes.value(index_fitxaInicial)->estat()==1){
       m_fitxes.value(index_fitxaInicial)->calculaMovimentsFitxa();
     qDebug("m_fitxes.value(index_fitxaInicial)->movimentsFitxa() %d",
            m_fitxes.value(index_fitxaInicial)->movimentsFitxa.count());
     if(m_fitxes.value(index_fitxaInicial)->movimentsFitxa.count()<1){
          m_fitxes.value(index_fitxaInicial)->setFitxaAillada(true);
          qDebug("Fitxa sense moviments!********");
       }
    }
    if(m_fitxes.value(index_fitxaInicial)->estat()==1){
        qDebug("m_fitxes.value(index_fitxaInicial)->movimentsFitxa() %d",
               m_fitxes.value(index_fitxaInicial)->movimentsFitxa.count());
    }

   // nivellAillamentFitxes=nivellAillamentFitxes+m_fitxes.value(index_fitxaInicial)->nivellAillament();
    }//final while
qDebug("p_numFitxesAillades %d", p_numFitxesAillades);
qDebug("numeroFitxesAillades %d", numeroFitxesAillades);
//pausa(3000);
return  (numeroFitxesAillades>p_numFitxesAillades);
//return  (numeroFitxesAillades>0);
 }
 */


 /* Nou procediment per a la versió 2.2
   Comprova si hi ha alguna fitxa "aïllada"
   o sigui fitxes en estat 1 que no tenen cap moviment
   que es pugui fer efectiu o be que no tenen
   altres fitxes (o buits en el cas de la modalitat inversa)
   que permetin que es moguin

   Es fa servir per controlar la marxa del joc marcant la situació
   de les fitxes.
   No es fa servir en l'executable final.
   */
 bool Tauler::solucionaJocActual_FitxesAillades1(){
     /* Aquest procediment no és eficient
      * amb la modalitat inversa
      03/09/16 A la versió 2.1 s'ha fet per la modalitat inversa */
     //if(p_tipusMoviment==2){return false;}
     solucionaJocActual_CalculaMovimentsPosibles();
     /* 22/08/16 Això no es fa servir per res*/
     //int nivellAillamentFitxes=0;
     int numeroFitxesAillades=0;
     //QHashIterator <int, Fitxa*> iterator_fitxes(m_fitxes);
     QMapIterator <int, Fitxa*> iterator_fitxes(m_fitxes);

     while ( iterator_fitxes.hasNext() ){
        iterator_fitxes.next();
        /* Calcula els moviments actuals (reals) de la fitxa*/
        iterator_fitxes.value()->calculaMovimentsFitxa();
        /* Només poden estar "aïllades" les fitxes actives sense moviments.
         * Poden estar aïllades les fitxes que son fitxes finals de joc*/
       if(  ((iterator_fitxes.value()->estat()==1) &&
            (iterator_fitxes.value()->movimentsFitxa.count()==0) ) &&
            (!(iterator_fitxes.value()->fitxaFinalJoc()))
            ){
           /* Es calcula la distància (del taxi) de la fitxa actual a altres fitxes
           * i també en diagonal si el moviment és en diagonal */
           QPoint distancies=distanciaDeUnaFitxaAAltres(iterator_fitxes.value()->coordenades());
           /* distancies.x() fitxes proximes
            * distancies.y() distància mínima */
           if(distancies.x()==0 || distancies.y() >= 3 ){
              iterator_fitxes.value()->setFitxaAillada(true);
            numeroFitxesAillades=numeroFitxesAillades+1;
            qApp->processEvents();
            //pausa(2000);
            }
           else iterator_fitxes.value()->setFitxaAillada(false);
         }//final del if inicial del while
     else {
       iterator_fitxes.value()->setFitxaAillada(false);
       }
    }//final while

return  (numeroFitxesAillades>=2);
//return  (numeroFitxesAillades>=p_numFitxesAillades);
 }

 /* Nou procediment per a la versió 2.1
    Comprova si hi ha alguna fitxa "aïllada"
    o sigui fitxes en estat 1 que no tenen cap moviment
    que es pugui fer efectiu o be que no tenen
    altres fitxes (o buits en el cas de la modialitat inversa)
    que permetin que es moguin
    */
  bool Tauler::solucionaJocActual_FitxesAillades2(){
      /* Aquest procediment no és eficient
       * amb la modalitat inversa
       03/09/16 A la versió 2.1 s'ha fet per la modalitat inversa */
      //if(p_tipusMoviment==2){return false;}
      solucionaJocActual_CalculaMovimentsPosibles();
      /* 22/08/16 Això no es fa servir per res*/
      //int nivellAillamentFitxes=0;
      int numeroFitxesAillades=0;
      //QHashIterator <int, Fitxa*> iterator_fitxes(m_fitxes);
      QMapIterator <int, Fitxa*> iterator_fitxes(m_fitxes);

      while ( iterator_fitxes.hasNext() ){
         iterator_fitxes.next();
         /* Calcula els moviments actuals (reals) de la fitxa*/
         iterator_fitxes.value()->calculaMovimentsFitxa();
         /* Només poden estar "aïllades" les fitxes actives sense moviments.
          * Poden estar aïllades les fitxes que son fitxes finals de joc*/
        if(  ((iterator_fitxes.value()->estat()==1) &&
             (iterator_fitxes.value()->movimentsFitxa.count()==0) ) &&
             (!(iterator_fitxes.value()->fitxaFinalJoc()))
             ){
            /* Es calcula la distància (del taxi) de la fitxa actual a altres fitxes
            * i també en diagonal si el moviment és en diagonal */
            QPoint distancies=distanciaDeUnaFitxaAAltres(iterator_fitxes.value()->coordenades(),3);
            /* distancies.x() fitxes proximes
             * distancies.y() distància mínima */
            if(distancies.x()<2 || distancies.y() > 2 ){
           // if(distancies.x()==0 || distancies.y() > 2 ){
             iterator_fitxes.value()->setFitxaAillada(true);
             numeroFitxesAillades=numeroFitxesAillades+1;
             qApp->processEvents();
             //pausa(2000);
             }
            else iterator_fitxes.value()->setFitxaAillada(false);
          }//final del if inicial del while
      else {
        iterator_fitxes.value()->setFitxaAillada(false);
        }
     }//final while

 //qDebug("p_numFitxesAillades %d", p_numFitxesAillades);
 //qDebug("numeroFitxesAillades %d", numeroFitxesAillades);
 //pausa(3000);
 return  (numeroFitxesAillades>qMax(3,p_numFitxesAillades));
 //return  (numeroFitxesAillades>qMax(p_numFitxesAillades,1));
 //return  (numeroFitxesAillades>0);
  }

  /* Retorna el nombre de fitxes aïllades i les marca en la propietat
   * de la fitxa*/
int Tauler::solucionaJocActual_numeroFitxesAillades(){
     solucionaJocActual_CalculaMovimentsPosibles();
     int numeroFitxesAillades=0;
     QMapIterator <int, Fitxa*> iterator_fitxes(m_fitxes);

     while ( iterator_fitxes.hasNext() ){
        iterator_fitxes.next();
        /* Calcula els moviments actuals (reals) de la fitxa*/
        iterator_fitxes.value()->calculaMovimentsFitxa();
        /* Només poden estar "aïllades" les fitxes actives sense moviments.
         * Poden estar aïllades les fitxes que son fitxes finals de joc*/
       if(  ((iterator_fitxes.value()->estat()==1) &&
            (iterator_fitxes.value()->movimentsFitxa.count()==0) ) &&
            (!(iterator_fitxes.value()->fitxaFinalJoc()))
            ){
           /* Es calcula la distància (del taxi) de la fitxa actual a altres fitxes
           * i també en diagonal si el moviment és en diagonal */
           QPoint distancies=distanciaDeUnaFitxaAAltres(iterator_fitxes.value()->coordenades());
           /* distancies.x() número de fitxes proximes
            * distancies.y() distància mínima */
           if(distancies.x()==0 || distancies.y() >= 3 ){
              iterator_fitxes.value()->setFitxaAillada(true);
            numeroFitxesAillades++;
            qApp->processEvents();
            //pausa(2000);
            }
           else iterator_fitxes.value()->setFitxaAillada(false);
         }//final del if inicial del while
     else {
       iterator_fitxes.value()->setFitxaAillada(false);
       }
    }//final while
return  numeroFitxesAillades;
 }

/* Per a cada fitxa, comprova si hi ha alguna altra fitxa pròxima
 * i assigna un nombre de conjunt a totes les fitxes pròximes entre si.
 * Retorna el nombre de conjunts del total de fitxes. En el cas de les
 * modalitats inverses, ho fa amb els buits*/
int Tauler::solucionaJocActual_FitxesAillades_PerConjunts(){
    /*Posam a zero les fitxes*/
    QMapIterator <int, Fitxa*> iterator_fitxes1(m_fitxes);
    while ( iterator_fitxes1.hasNext() ){
     iterator_fitxes1.next();
     iterator_fitxes1.value()->setConjuntAillament(0);
     iterator_fitxes1.value()->setDistanciaAillament(-1);
     iterator_fitxes1.value()->setDistanciaAillamentConjunt(20);
    }

    int estatFitxaInicial;
    //qDebug("p_tipusMoviment %d", p_tipusMoviment);
    /*Moviment invers*/
    if (p_tipusMoviment==2){
        estatFitxaInicial=0;
    }
    /*Moviment directe i diagonal*/
    else estatFitxaInicial=1;

     QMapIterator <int, Fitxa*> iterator_fitxes(m_fitxes);
     int contador=0;
     while ( iterator_fitxes.hasNext() ){
        iterator_fitxes.next();
        /* Només poden estar "aïllades" les fitxes actives (si el tipus de moviment
         * és directe) o els espais buits (si el tipus de moviment és invers).
         * Poden estar aïllades les fitxes que son fitxes finals de joc.
           Si s'ha arribat a una situació que ya no té més moviments (fitxes
           vermelles, també cal tenir-ho en compte*/
       if(  ((iterator_fitxes.value()->estat()==estatFitxaInicial ||
             iterator_fitxes.value()->estat()==5 )  &&
            (!(iterator_fitxes.value()->fitxaFinalJoc()))
            )){
           if(iterator_fitxes.value()->conjuntAillament()==0){
              contador++;
              iterator_fitxes.value()->setConjuntAillament(contador);
           }
           fitxesProximesAUnAltra(iterator_fitxes.value()->coordenades(),contador,contador);
       }
    }//final while
     iterator_fitxes1.toFront();
     QList<int> conjunts;

     while ( iterator_fitxes1.hasNext() ){
      iterator_fitxes1.next();
      if(  ( (iterator_fitxes1.value()->estat()==estatFitxaInicial || iterator_fitxes1.value()->estat()==5 )  &&
           (!(iterator_fitxes1.value()->fitxaFinalJoc()))
           )){
           if (!conjunts.contains(iterator_fitxes1.value()->conjuntAillament())){
              conjunts.append(iterator_fitxes1.value()->conjuntAillament());
           }
           fitxaDistanciaAillament(iterator_fitxes1.value()->coordenades(),iterator_fitxes1.value() );
      }
     }

//solucionaJocActual_conjuntsAillats(conjunts.count());
return conjunts.count();
}

/* Retorna el nombre de conjunts dispesos: conjunts de fitxes que estan
 * massa aïllats dels altres conjunts*/
int Tauler::solucionaJocActual_conjuntsDispersos(int numeroConjuntsActual){
QMapIterator <int, Fitxa*> iterator_fitxes(m_fitxes);
QList<int> conjuntsDispersos;
for(int c=0 ; c<numeroConjuntsActual;c++){
   conjuntsDispersos.append(0);
}

int fitxaMasssaAillada4=0;
int fitxaMasssaAillada3=0;
int fitxaMasssaAillada2=0;
  while ( iterator_fitxes.hasNext() ){
   iterator_fitxes.next();
   if(iterator_fitxes.value()->distanciaAillament()>1){
//     qDebug("conjuntsDispersos %d", conjuntsDispersos.count());
     conjuntsDispersos.replace(iterator_fitxes.value()->conjuntAillament()-1,
       conjuntsDispersos.value(iterator_fitxes.value()->conjuntAillament()-1)+1);
   }
   if(iterator_fitxes.value()->distanciaAillament()>4){
     fitxaMasssaAillada4++;
   }
   else if (iterator_fitxes.value()->distanciaAillament()==3){
       fitxaMasssaAillada3++;
     }
   else if (iterator_fitxes.value()->distanciaAillament()==2){
       fitxaMasssaAillada2++;
     }
  }

int numeroConjuntsDispersos=0;
for(int j=0 ; j<conjuntsDispersos.count();j++){
   //Si hi ha més de dues fitxes que tenen una distància superior a 1
   if (conjuntsDispersos.value(j)>2){
     numeroConjuntsDispersos++;
   }
  }
//qDebug("numeroConjuntsDispersos %d", numeroConjuntsDispersos);
//if(numeroConjuntsDispersos>1){
//  pausa(2000);
//}
if (fitxaMasssaAillada4>0 || fitxaMasssaAillada3>2 || fitxaMasssaAillada2>5){
//  numeroConjuntsDispersos=10;
  numeroConjuntsDispersos=numeroConjuntsDispersos+1;
}
return numeroConjuntsDispersos;
}

/* Calcula la distància mínima entre conjunts de fitxes
 */
int Tauler::solucionaJocActual_conjuntsAillats(int numeroConjuntsActual){
//no val la pena fer res en aquest cas!
if(numeroConjuntsActual<=1){
  return 0;
}
QMapIterator <int, Fitxa*> iterator_fitxes(m_fitxes);
QMapIterator <int, Fitxa*> iterator_fitxes2(m_fitxes);
//int contador1=0;
//int contador2=0;
 while ( iterator_fitxes.hasNext() ){
    iterator_fitxes.next();
//    contador1++;
    if ( (p_tipusMoviment == 2 && iterator_fitxes.value()->estat()==0)
     || (iterator_fitxes.value()->estat()==1) ){
    while ( iterator_fitxes2.hasNext() ){
    iterator_fitxes2.next();
//    contador2++;
    if ( (p_tipusMoviment == 2 && iterator_fitxes2.value()->estat()==0)
     || (iterator_fitxes2.value()->estat()==1) ){
    if( iterator_fitxes.value()->conjuntAillament() != iterator_fitxes2.value()->conjuntAillament()){
      int distancia=distanciaEntreDuesFitxes(iterator_fitxes.value()->coordenades(),
                               iterator_fitxes2.value()->coordenades());
      if(iterator_fitxes.value()->distanciaAillamentConjunt()>distancia){
        iterator_fitxes.value()->setDistanciaAillamentConjunt(distancia);
       }
      if(iterator_fitxes2.value()->distanciaAillamentConjunt()>distancia){
        iterator_fitxes2.value()->setDistanciaAillamentConjunt(distancia);
       }
      }
     }
    }
   }
 iterator_fitxes2.toFront();
 }

iterator_fitxes.toFront();
int distanciaMaximaDeTotsElsConjunts=0;
for(int j=0 ; j<numeroConjuntsActual;++j){
  int distanciaMinimaDelConjunt=1000;
  while ( iterator_fitxes.hasNext() ){
   iterator_fitxes.next();
   if(iterator_fitxes.value()->conjuntAillament()==j+1){
     if(iterator_fitxes.value()->distanciaAillamentConjunt()<distanciaMinimaDelConjunt) {
       distanciaMinimaDelConjunt=iterator_fitxes.value()->distanciaAillamentConjunt();
     }
   }
  }
 iterator_fitxes.toFront();
 if (distanciaMaximaDeTotsElsConjunts<distanciaMinimaDelConjunt){
  distanciaMaximaDeTotsElsConjunts=distanciaMinimaDelConjunt;
  }
 }
if(distanciaMaximaDeTotsElsConjunts==20){
 distanciaMaximaDeTotsElsConjunts=0;
}
//qDebug("distanciaMaximaDeTotsElsConjunts %d",distanciaMaximaDeTotsElsConjunts);
//qApp->processEvents();
//pausa(4000);
return distanciaMaximaDeTotsElsConjunts;
}

/* Calcula la distància entre una fitxa i les altres per saber
 * si està aïllada */
QPoint Tauler::distanciaDeUnaFitxaAAltres(QPoint coordenades, int proximitat){
QMapIterator <int, Fitxa*> iterator_fitxes(m_fitxes);
//int proximitat=3;
int fitxesProximes=0;
int distanciaMinima=10000;
//qDebug("Fitxa %d %d", coordenades.x(),coordenades.y());
while ( iterator_fitxes.hasNext() ){
   iterator_fitxes.next();
   QPoint coordenadesAltraFitxa;
   bool continuar=false;
   /* només per a fitxes actives*/
   if(p_tipusMoviment == 2 &&  /* Moviment invers*/
      iterator_fitxes.value()->estat()==0){
      coordenadesAltraFitxa=iterator_fitxes.value()->coordenades();
      continuar=true;
   }
   else if (iterator_fitxes.value()->estat()==1){
   coordenadesAltraFitxa=iterator_fitxes.value()->coordenades();
   continuar=true;
    }

   if (continuar){
   //qDebug("Altra fitxa %d %d", coordenadesAltraFitxa.x(),coordenadesAltraFitxa.y());
   /* No es calcula la distància d'un fitxa a si mateixa */
   if(coordenades != coordenadesAltraFitxa){
   int distancia=abs(coordenades.x()-coordenadesAltraFitxa.x())+
                 abs(coordenades.y()-coordenadesAltraFitxa.y());
   if (p_tipusMoviment==3){/* cas de moviments en diagonal
      * també es calcula les distàncies en diagonal*/
     if(abs(coordenades.x()-coordenadesAltraFitxa.x())==
         abs(coordenades.y()-coordenadesAltraFitxa.y()) ){/*Les fitxes estan en la mateixa diagonal*/
          int distanciaEnDiagonal=abs(coordenades.x()-coordenadesAltraFitxa.x());
          /* en aquest cas, s'agafa la distància més curta */
          distancia=qMin(distancia,distanciaEnDiagonal);
     }
   }/* if moviment en diagonal 3*/
   distanciaMinima=qMin(distanciaMinima,distancia);
   //qDebug("distancia %d",distancia);
   if(distancia<=proximitat || distanciaMinima <= proximitat){
     fitxesProximes++;
     }
    }/* if de igualtat de coordenades*/
  }//if continuar
}/* final del while */

/*qDebug("fitxesProximes %d",fitxesProximes);
qDebug("distanciaMinima %d",distanciaMinima);*/
return QPoint(fitxesProximes,distanciaMinima);
}

/* Comprova quines fitxes estan pròximes a la passada per paràmetre
 * i assigna un número de conjunt.*/
void Tauler::fitxesProximesAUnAltra(QPoint coordenadesFitxaOriginal,
                                    int numConjuntAillament, int numContador ){
    int proximitat=2;
   // int distanciaMinima=10000;

  QMapIterator <int, Fitxa*> iterator_fitxes(m_fitxes);
  while ( iterator_fitxes.hasNext() ){
     iterator_fitxes.next();
     QPoint coordenadesAltraFitxa;
     bool continuar=false;
     if( (p_tipusMoviment == 2 || p_tipusMoviment == 5 ) &&  /* Moviment invers i invers en diagonal */
        (iterator_fitxes.value()->estat()==0 || iterator_fitxes.value()->estat()==5) ){
       // qDebug("espai buit");
        coordenadesAltraFitxa=iterator_fitxes.value()->coordenades();
        continuar=true;
     }
     else if ( (p_tipusMoviment == 1 || p_tipusMoviment == 3 ) && /* Moviment directe i directe en diagonal */
              (iterator_fitxes.value()->estat()==1 || iterator_fitxes.value()->estat()==5) ){
     //qDebug("fitxa activa");
     coordenadesAltraFitxa=iterator_fitxes.value()->coordenades();
     continuar=true;
     }

     if (continuar && coordenadesAltraFitxa != coordenadesFitxaOriginal){
     int distancia=abs(coordenadesFitxaOriginal.x()-coordenadesAltraFitxa.x())+
                   abs(coordenadesFitxaOriginal.y()-coordenadesAltraFitxa.y());
     if (p_tipusMoviment==3){/* cas de moviments en diagonal
        * també es calcula les distàncies en diagonal*/
        if(abs(coordenadesFitxaOriginal.x()-coordenadesAltraFitxa.x())==
           abs(coordenadesFitxaOriginal.y()-coordenadesAltraFitxa.y()) ){
            /*Les fitxes estan en la mateixa diagonal*/
            /*Les fitxes en diagonal, poden tenir fitxes «buides» (estat -1) enmig*/
            int distanciaEnDiagonal=abs(coordenadesFitxaOriginal.x()-coordenadesAltraFitxa.x());
            /* en aquest cas, s'agafa la distància més curta */
            distancia=qMin(distancia,distanciaEnDiagonal);
       }
     }/* if moviment en diagonal 3*/
     //distanciaMinima=qMin(distanciaMinima,distancia);
     //if(distancia<=proximitat || distanciaMinima < 3){
     if(distancia<=proximitat ){
       /* Afegim la fitxa al conjunt de fitxes pròximes*/
       if(iterator_fitxes.value()->conjuntAillament() == 0){
           iterator_fitxes.value()->setConjuntAillament(numConjuntAillament);
           fitxesProximesAUnAltra(iterator_fitxes.value()->coordenades(),numConjuntAillament,numContador);
        }
       }
     // }/* if de igualtat de coordenades*/
    }//if continuar
  }//final while
  qApp->processEvents();
}

/*Versió del procediment anterior*/
void Tauler::fitxaDistanciaAillament(QPoint coordenadesFitxaOriginal, Fitxa* fitxaOriginal){

    QMapIterator <int, Fitxa*> iterator_fitxes(m_fitxes);
    while ( iterator_fitxes.hasNext() ){
       iterator_fitxes.next();
       QPoint coordenadesAltraFitxa;
       bool continuar=false;
       if( (p_tipusMoviment == 2 || p_tipusMoviment == 5 ) &&  /* Moviment invers i invers en diagonal */
          (iterator_fitxes.value()->estat()==0 || iterator_fitxes.value()->estat()==5) ){
         // qDebug("espai buit");
          coordenadesAltraFitxa=iterator_fitxes.value()->coordenades();
          continuar=true;
       }
       else if ( (p_tipusMoviment == 1 || p_tipusMoviment == 3 ) && /* Moviment directe i directe en diagonal */
                (iterator_fitxes.value()->estat()==1 || iterator_fitxes.value()->estat()==5) ){
       //qDebug("fitxa activa");
       coordenadesAltraFitxa=iterator_fitxes.value()->coordenades();
       continuar=true;
       }

       if (continuar && coordenadesAltraFitxa != coordenadesFitxaOriginal){
       int distancia=abs(coordenadesFitxaOriginal.x()-coordenadesAltraFitxa.x())+
                     abs(coordenadesFitxaOriginal.y()-coordenadesAltraFitxa.y());
       if (p_tipusMoviment==3){/* cas de moviments en diagonal
          * també es calcula les distàncies en diagonal*/
          if(abs(coordenadesFitxaOriginal.x()-coordenadesAltraFitxa.x())==
             abs(coordenadesFitxaOriginal.y()-coordenadesAltraFitxa.y()) ){
              /*Les fitxes estan en la mateixa diagonal*/
              /*Les fitxes en diagonal, poden tenir fitxes «buides» (estat -1) enmig*/
              int distanciaEnDiagonal=abs(coordenadesFitxaOriginal.x()-coordenadesAltraFitxa.x());
              /* en aquest cas, s'agafa la distància més curta */
              distancia=qMin(distancia,distanciaEnDiagonal);
         }
       }/* if moviment en diagonal 3*/
     // qDebug("distancia %d %d", distancia,fitxaOriginal->distanciaAillament());
       if(fitxaOriginal->distanciaAillament()==-1 ||
          (distancia<fitxaOriginal->distanciaAillament() &&
           distancia != 0)  ){
         fitxaOriginal->setDistanciaAillament(distancia);
         //qDebug("dins if distancia %d %d", distancia,fitxaOriginal->distanciaAillament());
       }

       // }/* if de igualtat de coordenades*/
      }//if continuar
    }//final while
    qApp->processEvents();
}

/* Calcula la distància entre dues fitxes a partir de les seves coordenades*/
int Tauler::distanciaEntreDuesFitxes(QPoint coordenades1, QPoint coordenades2){
    int distancia=abs(coordenades1.x()-coordenades2.x())+
                  abs(coordenades1.y()-coordenades2.y());
//    qDebug("coordenades1 %d %d",coordenades1.x(),coordenades1.y());
//    qDebug("coordenades2 %d %d",coordenades2.x(),coordenades2.y());
//    qDebug("distancia a %d", distancia);
    if (p_tipusMoviment==3){
        if(abs(coordenades1.x()-coordenades2.x())==
           abs(coordenades1.y()-coordenades2.y()) ){
            /*Les fitxes estan en la mateixa diagonal*/
            /*Les fitxes en diagonal, poden tenir fitxes «buides» (estat -1) enmig*/
            int distanciaEnDiagonal=abs(coordenades1.x()-coordenades2.x());
            /* en aquest cas, s'agafa la distància més curta */
            distancia=qMin(distancia,distanciaEnDiagonal);
        }
       }
// qDebug("distancia b %d", distancia);
return distancia;
}

 /*
   Comprova si hi ha algun moviment posible que tengui com a fitxa final
   la fitxa passada per paràmetre.
   No es fa servir
   */
bool Tauler::movimentPosibleFitxaFinal(int fitxaMijana){
  bool resultat=false;
 QMapIterator <int, Fitxa*> iterator_fitxes(m_fitxes);
 //QHashIterator <int, Fitxa*> iterator_fitxes(m_fitxes);

      while (iterator_fitxes.hasNext() && (! resultat) ) {
        iterator_fitxes.next();
        int index_fitxaInicial=iterator_fitxes.value()->coordenades().x()*100+iterator_fitxes.value()->coordenades().y();
        for(int j=0 ; j<iterator_fitxes.value()->movimentsPossibles.count();++j){
             if  (resultat){break;}
            int index_fitxaBotada=iterator_fitxes.value()->movimentsPossibles.at(j).x();
            int index_fitxaFinal=iterator_fitxes.value()->movimentsPossibles.at(j).y();
            if (  (m_fitxes.value(index_fitxaInicial)->estat()==1) &&
                (m_fitxes.value(index_fitxaBotada)->estat()==p_estatFitxaMitjana) &&
                ( index_fitxaFinal==fitxaMijana) ){
                resultat=true;
               }
            //miram si la fitxa botada és el final d'un moviment posible
            else if (  (m_fitxes.value(index_fitxaInicial)->estat()==1) &&
                !(m_fitxes.value(index_fitxaBotada)->estat()==p_estatFitxaMitjana) &&
                ( index_fitxaFinal==fitxaMijana) ){
                 if (movimentPosibleFitxaFinal1(index_fitxaBotada) ) {
                resultat=true;}
               }
            }
        }
  return resultat;
 }

/* No es fa servir*/
 bool Tauler::movimentPosibleFitxaFinal1(int fitxaMijana){
  bool resultat=false;
  //QHashIterator <int, Fitxa*> iterator_fitxes(m_fitxes);
  QMapIterator <int, Fitxa*> iterator_fitxes(m_fitxes);

      while (iterator_fitxes.hasNext() && (! resultat) ) {
        iterator_fitxes.next();
        int index_fitxaInicial=iterator_fitxes.value()->coordenades().x()*100+iterator_fitxes.value()->coordenades().y();
        for(int j=0 ; j<iterator_fitxes.value()->movimentsPossibles.count();++j){
             if  (resultat){break;}
            int index_fitxaBotada=iterator_fitxes.value()->movimentsPossibles.at(j).x();
            int index_fitxaFinal=iterator_fitxes.value()->movimentsPossibles.at(j).y();
            if (  (m_fitxes.value(index_fitxaInicial)->estat()==1) &&
                (m_fitxes.value(index_fitxaBotada)->estat()==p_estatFitxaMitjana) &&
                ( index_fitxaFinal==fitxaMijana) ){
                resultat=true;
               }
            }
        }
  return resultat;
 }

 /*
   Comprova si hi ha algun moviment possible que tengui la fitxa
   passada per paràmetre com a fitxa botada.
   No es fa servir
   */
 bool Tauler::movimentPosibleFitxaMitjana(int fitxaMitjana){
     bool resultat=false;
     //QHashIterator <int, Fitxa*> iterator_fitxes(m_fitxes);
     QMapIterator <int, Fitxa*> iterator_fitxes(m_fitxes);

      while (iterator_fitxes.hasNext() && (! resultat)) {
        iterator_fitxes.next();
        int index_fitxaInicial=iterator_fitxes.value()->coordenades().x()*100+iterator_fitxes.value()->coordenades().y();
        for(int j=0 ; j<iterator_fitxes.value()->movimentsPossibles.count();++j){
            if  (resultat){break;}
            int index_fitxaBotada=iterator_fitxes.value()->movimentsPossibles.at(j).x();
            //int index_fitxaFinal=iterator_fitxes.value()->movimentsPossibles.at(j).y();
            if (  (m_fitxes.value(index_fitxaInicial)->estat()==1) &&
                (m_fitxes.value(index_fitxaBotada)->estat()==p_estatFitxaMitjana) &&
                ( index_fitxaBotada==fitxaMitjana) ){
                resultat=true;
               }
            }
        }
  return resultat;
 }

 /*
   No es fa servir
   Determina si la posició central

 bool Tauler::movimentFitxaCentral(){
     //QPoint(3,3);
     bool resultat=false;
     int fitxaCentral=303;
     QHashIterator <int, Fitxa*> iterator_fitxes(m_fitxes);
      while (iterator_fitxes.hasNext() && !resultat ) {
        iterator_fitxes.next();
        int index_fitxaInicial=iterator_fitxes.value()->coordenades().x()*100+iterator_fitxes.value()->coordenades().y();
        for(int j=0 ; j<iterator_fitxes.value()->movimentsPossibles.count();++j){            
            int index_fitxaBotada=iterator_fitxes.value()->movimentsPossibles.at(j).x();
            if (  (m_fitxes.value(index_fitxaInicial)->estat()==1) &&
                (m_fitxes.value(index_fitxaBotada)->estat()==p_estatFitxaMitjana) &&
                fitxaCentral==iterator_fitxes.value()->movimentsPossibles.at(j).y() ){
             //   qDebug("fitxa central SI");
                resultat= true;
               }
            }
        }
// qDebug("fitxa central NO");
return resultat;
 }
 */

 /* Posa totes les fitxes vermelles
  * a color blau
  */
 void Tauler::eliminaFitxesVermelles(){
    //QHashIterator <int, Fitxa*> i(m_fitxes);
    QMapIterator <int, Fitxa*> i(m_fitxes);
    while (i.hasNext()) {
        i.next();
        if (i.value()->estat()==5){
            i.value()->setEstat(1);
        }
     }
}

/* Posa totes les fitxes actives
 * a color blau
 */
 void Tauler::eliminaFitxesVerdes(){
    //QHashIterator <int, Fitxa*> i(m_fitxes);
    QMapIterator <int, Fitxa*> i(m_fitxes);
    while (i.hasNext()) {
        i.next();
        if (i.value()->estat()==4){
            i.value()->setEstat(1);
        }
     }
}

QString Tauler::configuracioActual(bool comptarFitxes){

    // QHashIterator <int, Fitxa*> p(m_fitxes);
     QMapIterator <int, Fitxa*> p(m_fitxes);
     QString configuracioActual;
     if (comptarFitxes){
         comptadorFitxesJoc=0;
         comptadorFitxesEstatZeroJoc=0;
         comptadorFitxesFinalJoc=0;
     }

        int contador=0;
        while (p.hasNext()) {
              p.next();
              configuracioActual.append(QString("%1").arg((p.value()->estat())));
              //qCritical("configuracioActual %s",qPrintable(configuracioActual));
              //qDebug("Key %d estat %d",p.key(),p.value()->estat());

              if (comptarFitxes){
               if (p.value()->estat()==1){++comptadorFitxesJoc;}
               if (p.value()->estat()==0){++comptadorFitxesEstatZeroJoc;}
               if (p.value()->fitxaFinalJoc()){
                  ++comptadorFitxesFinalJoc;
                  }
              }
           ++contador;
          }
/* Això cal quan es modifica un joc del programa
 * i s'han eliminat fitxes.
 */
configuracioActual.remove("6");
//qDebug("contador %d",contador);
//qCritical("configuracioActual %s",qPrintable(configuracioActual));
return configuracioActual;
 }

/* Retorna la configuració actual de les fitxes amb les
 * fitxes finals de joc marcades amb -
 * 10/02/13 Al final no es fa servir

QString Tauler::configuracioInicialAmbFitxesMarcades(){
 QHashIterator <int, Fitxa*> i(m_fitxes);
 QString configuracioActual;
 while (i.hasNext()) {
       i.next();
       configuracioActual.append(QString("%1").arg((i.value()->estat())));
       if (i.value()->fitxaFinalJoc()){
           configuracioActual.append("-");
       }
   }
return configuracioActual;
}*/


//Carrega la configuració passada
void Tauler::carregaConfiguracio(QString configuracio){
     //QHashIterator <int, Fitxa*> i(m_fitxes);
     QMapIterator <int, Fitxa*> i(m_fitxes);
     int contador=0;
     while (i.hasNext()) {
              i.next();
              QString valor=configuracio.at(contador);
              i.value()->setEstat(valor.toInt());
              contador ++;
          }
 }

 /*
Canvia l'estat de les fitxes a l'atzar
Es fa servir quan el joc està en pausa
NO es fa servir: es va eliminar per la sensibilitat
al color
*/
//void Tauler::estatFitxesAtzar(){
//    qsrand(QTime::currentTime().msec());
//     //QHashIterator <int, Fitxa*> i(m_fitxes);
//     QMapIterator <int, Fitxa*> i(m_fitxes);
//     while (i.hasNext()) {
//              i.next();
//              i.value()->setEstat(qrand() % 5);
//          }
// }

 /*
   Retorna el número de moviments que cal fer en el joc
   (en funció del tipus de moviment)
   */
 int Tauler::numeroDeMovimentsJoc(){
    /* qDebug("p_tipusMoviment %d",p_tipusMoviment);
     qDebug("comptadorFitxesFinalJoc %d",comptadorFitxesFinalJoc);
     qDebug("comptadorFitxesJoc %d",comptadorFitxesJoc);
     qDebug("comptadorFitxesFinalJoc %d",comptadorFitxesFinalJoc);*/
     if (p_tipusMoviment !=2){
         if(comptadorFitxesFinalJoc<2){
           return comptadorFitxesJoc-1;
         }
         else
          return comptadorFitxesJoc-comptadorFitxesFinalJoc;
      }
       // qDebug("num, mov. %d",comptadorFitxesJoc-qMax(1,comptadorFitxesFinalJoc));
        // return comptadorFitxesJoc-qMax(1,comptadorFitxesFinalJoc);}
     else {
        // qDebug("m_fitxes.count() %d",m_fitxes.count());
        // qDebug("comptadorFitxesJoc %d",comptadorFitxesJoc);
        // 10/01/13 modificat
        return comptadorFitxesEstatZeroJoc-1;
     }
 }

 /*
   Retorna el missatge del número de moviments realitzats i el total
   No es fa servir

 QString Tauler::missatgeNumeroMoviments(){
    return QString(tr("Moviment %1 de %2 ")).arg(p_movimentsUndoStack->index()).arg(numeroDeMovimentsJoc());
 }
 */


 void Tauler::setModalitatJocActual(QString valor){
   p_modalitatJocActual=valor;
   setSolitariEspecial(p_modalitatJocActual);
 }

 void Tauler::setEstatDelJoc(EstatsDelJoc valor){
     if(p_estatDelJoc != valor){
      p_estatDelJoc=valor;
    }
 }

 /* Conserva el codi (per la traducció)
  * del joc actual
  */
 void Tauler::setCodiNomJocActual (QString codi){
     p_codiNomJocActual=codi;
 }

 /* En la generació d'un joc a l'atzar
  * marca les fitxes que s'ha fet servir per
  * generar el joc
  */
 void Tauler::solitariAtzar_MarcaFitxaUsada(int fitxa,bool marcaFitxaFinal){
   m_fitxes.value(fitxa)->setfitxaUsadaGeneracioSolitariAtzar(true);
   m_fitxes.value(fitxa)->setFitxaFinalJoc(marcaFitxaFinal);
 }


 /* En la generació d'un joc a l'atzar
  * retorna l'estat de les fitxes del
  * joc generat a l'atzar
  */
 QString Tauler::solitariAtzar_dadesEstatFitxes(int files, int columnes){
     //QHashIterator <int, Fitxa*> i(m_fitxes);
     QMapIterator <int, Fitxa*> i(m_fitxes);
     QString dadesFitxes;
     QStringList dadesFitxesList;
     //omplim la llista
     for(int j=0; j<files*columnes; j++){
         dadesFitxesList.append("-2");
     }
     i.toFront();
     while (i.hasNext()) {
              i.next();
              //Si la fitxa està marcada (s'ha fet
              //servir en la generació del joc)
              if (i.value()->fitxaUsadaGeneracioSolitariAtzar()){
                  //Si està marcada com a final del joc
                  if(i.value()->fitxaFinalJoc()){
                      dadesFitxesList.replace(
                        i.value()->coordenades().x()*columnes+
                        i.value()->coordenades().y(),
                        QString("1%1").arg(i.value()->estat()));
                  }
                  else dadesFitxesList.replace(
                         i.value()->coordenades().x()*columnes+
                         i.value()->coordenades().y(),
                         QString("%1").arg(i.value()->estat()));
              }
              else {dadesFitxesList.replace(
                          i.value()->coordenades().x()*columnes+
                          i.value()->coordenades().y(),
                          QString("-1"));
              }
          }
   dadesFitxes.clear();
   //passam les dades de la llista a una cadena
   for(int j=0; j<dadesFitxesList.count(); j++){
       dadesFitxes=dadesFitxes+dadesFitxesList.value(j)+" ";
   }
   return dadesFitxes.simplified();
 }

 /* Estableix si s'està jugant un solitari a l'atzar
  */
 void Tauler::setSolitariAtzar(bool valor){
  p_SolitariAtzar=valor;
 }

 /* Retorna les files i columnes, i les dades de les
  * fitxes d'un solitari personalitzat
  */
 QStringList Tauler::solitariPersonalitzat_dadesFitxes(){
 //calculem les files i columnes
 QStringList filesColumnes=solitariPersonalitzat_filesColumnes().split(" ");
 int minFila=filesColumnes.value(0).toInt();
 int maxFila=filesColumnes.value(1).toInt();
 int files=maxFila-minFila+1;
 int minCol=filesColumnes.value(2).toInt();
 int maxCol=filesColumnes.value(3).toInt();
 int columnes=maxCol-minCol+1;

 QStringList dadesFitxesList;
 //omplim la llista
 for(int j=0; j<files*columnes; j++){
     dadesFitxesList.append("-1");
 }
 //QHashIterator <int, Fitxa*> i(m_fitxes);
 QMapIterator <int, Fitxa*> i(m_fitxes);
 i.toFront();
 while (i.hasNext()) {
          i.next();
          if( (i.value()->coordenades().x()>=minFila) &&
              (i.value()->coordenades().x()<=maxFila) &&
              (i.value()->coordenades().y()>=minCol) &&
              (i.value()->coordenades().y()<=maxCol) ){
              //els estats 6 no cal reemplaçar-los
              //(ja hi ha -1 per defecte)
              if (i.value()->estat() != 6){
                 dadesFitxesList.replace(
                     (i.value()->coordenades().x()-minFila)*columnes+
                      i.value()->coordenades().y()-minCol,
                      QString("%1").arg(i.value()->estat()));
                 //Això per les fitxes marcades com a final de joc
                 if(i.value()->fitxaFinalJoc()){
                   dadesFitxesList.replace(
                         (i.value()->coordenades().x()-minFila)*columnes+
                          i.value()->coordenades().y()-minCol,
                          QString("1%1").arg(i.value()->estat()));
                 }
              }
          }
    }//final while
 //passam les dades de la llista a una cadena
 QString dadesFitxes;
 for(int j=0; j<dadesFitxesList.count(); j++){
     dadesFitxes=dadesFitxes+dadesFitxesList.value(j)+" ";
   }
 dadesFitxesList.clear();
 dadesFitxesList.append(QString("%1 %2").arg(files).arg(columnes));
 dadesFitxesList.append(dadesFitxes.simplified());
 //qCritical("Tauler::solitariPersonalitzat_dadesFitxes\n %s", qPrintable(dadesFitxes));
 return dadesFitxesList;
 }

 /* Retorna el número de files i columnes del joc
  * personalitzat.
  * La llista té les coordenades més petita i més gran
  * de files i columnes.
  */
 QString Tauler::solitariPersonalitzat_filesColumnes(){
  //QHashIterator <int, Fitxa*> i(m_fitxes);
  QMapIterator <int, Fitxa*> i(m_fitxes);
  int minFila=1000, maxFila=0,minCol=1000,maxCol=0;
  while (i.hasNext()) {
     i.next();
     //Si la fitxa s'ha fet servir
     if(i.value()->estat() != 6){
         minFila=qMin(minFila,i.value()->coordenades().x()) ;
         maxFila=qMax(maxFila,i.value()->coordenades().x()) ;
         minCol=qMin(minCol,i.value()->coordenades().y()) ;
         maxCol=qMax(maxCol,i.value()->coordenades().y()) ;
     }
  }
 QString filesColumnes;
 filesColumnes.append(QString("%1 %2 ").arg(minFila).arg(maxFila));
 filesColumnes.append(QString("%1 %2").arg(minCol).arg(maxCol));
 //QString missatge=QString(filesColumnes.value(0)+" "+filesColumnes.value(1));
 //qCritical("%s", qPrintable(filesColumnes));
 return filesColumnes;
 }

 /* Estableix si s'està jugant un solitari personalitzat
  */
 void Tauler::setSolitariPersonalitzat(bool valor){
  p_SolitariPersonalitzat=valor;
 }

 /* Estableix si s'està jugant un solitari modificat
  */
 void Tauler::setSolitariModificat(bool valor){
  p_SolitariModificat=valor;
 }

 /* Marcam totes les fitxes com a personalitzades
  * (segons el valor passat)
  */
 void Tauler::marcaFitxesSolitariPersonalitzat(bool valor){
     QMapIterator <int, Fitxa*> i(m_fitxes);
     //QHashIterator <int, Fitxa*> i(m_fitxes);
     while (i.hasNext()) {
        i.next();
        i.value()->setFitxaSolitariPersonalitzat(valor);
     }
 }

 /* Comprova si hi ha alguna fitxa del solitari marcada
  * com a personalitzada
  * 10/02/13 Finalment, no es fa servir això

 bool Tauler::solitariPersonalitzat_hiHaFitxesMarcades(){
  QHashIterator <int, Fitxa*> i(m_fitxes);
  bool valor=false;
  while (i.hasNext()) {
    i.next();
    if(i.value()->fitxaSolitariPersonalitzat()){
      valor=true;
      break;
     }
    }
  return valor;
 }*/
