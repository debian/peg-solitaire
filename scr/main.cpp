/***********************************************************************
 *
 * Copyright (C) 2010-2017 Innocent De Marchi <tangram.peces@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ***********************************************************************/
#include <QApplication>
#include <QTranslator>
#include <QTextCodec>
#include <QLocale>
#include <QtCore>
#include <QtWidgets/QSplashScreen>
#include "frmprincipal.h"
#include "funcions.h"

 QTranslator *qt_translator;
 QTranslator *appTranslator;

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
        app.setApplicationName("Solitari");
        app.setApplicationVersion("2.2");
        app.setOrganizationName("De Marchi");

        //Això pels accents a les cadenes
        QTextCodec *linuxCodec = QTextCodec::codecForName("UTF-8");
       // QTextCodec::setCodecForTr(linuxCodec);
       // QTextCodec::setCodecForCStrings(linuxCodec);
        QTextCodec::setCodecForLocale(linuxCodec);

        QPixmap pixmap(directoriLinux()+QDir().separator()+"images"+QDir().separator()+"asimetric_jugant.png");
        QSplashScreen splash(pixmap);
        splash.show();
        //Sense aquesta línia,la imatge no s'actualitza
        splash.showMessage("");

    //Això per a que surtin traduïdes les cadenes dels formularis predefinits Qt
     QString qt_translatorFileName = QLatin1String("qt_");
     qt_translatorFileName += QLocale::system().name();

     qt_translator = new QTranslator();
     if (qt_translator->load(qt_translatorFileName, QLibraryInfo::location(QLibraryInfo::TranslationsPath)))
         app.installTranslator(qt_translator);
     //15/02/15 A windows cal posar
     /*if (qt_translator->load(qt_translatorFileName, QApplication::applicationDirPath()))
      app.installTranslator(translator);*/

   //Això per les traduccions
   appTranslator= new QTranslator();
   appTranslator->load("solitari_" + QLocale::system().name(), app.applicationDirPath()+QDir().separator()+"locales");

   app.installTranslator(appTranslator);

    frmPrincipal *frmMDI=new frmPrincipal();
    frmMDI->show();

    splash.finish(frmMDI);
    return app.exec();
}
