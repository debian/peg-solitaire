/***********************************************************************
 *
 * Copyright (C) 2010-2017 Innocent De Marchi <tangram.peces@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ***********************************************************************/
//#include <QTest>

#include "funcions.h"
#include "constants.h"



/*
  Directoris linux
  21/04/11
  Modificat per permetre la coexistència de l'aplicació instal·lada
  i el codi font
 */
QString directoriLinux(){  
    QDir dirArxiu;
     if (dirArxiu.exists(QCoreApplication::applicationDirPath()+ QDir().separator()+
                         "images")){
         return QCoreApplication::applicationDirPath();
     }
     else if (dirArxiu.exists(DIRECTORIS_LINUX)){
         return DIRECTORIS_LINUX;
      }
     else return QCoreApplication::applicationDirPath();
 /*
 QString dirInicial=QCoreApplication::applicationDirPath().replace(DIRECTORIS_LINUX_PROGRAMA,"");
 dirInicial=dirInicial.append(DIRECTORIS_LINUX_AUXILIARS);
 QDir dirArxiu;
    if (dirArxiu.exists(DIRECTORIS_LINUX)){
       return DIRECTORIS_LINUX;
    }
    else if (dirArxiu.exists(dirInicial)){
        return dirInicial;
    }
    else return QCoreApplication::applicationDirPath();
    */
}


/*
  Número de fitxes de la configuració passada per parámetre
  */
int numeroConfiguracio(QString config){
  int numero=0;
     for(int llista=0 ; llista<config.size();++llista){
         numero=numero+config.mid(llista,1).toInt();
     }
   //  qDebug("numero configuracio %d",numero);
     return numero;
}

QString coordenadesAMoviment(QString coordenades){
  QRegExp coord(".(\\d+),(\\d+). -> .(\\d+),(\\d+).");
  if (coord.exactMatch(coordenades)){
      int fitxaBotada=((coord.cap(1).toInt()+coord.cap(3).toInt())/2)*100+
                      ((coord.cap(2).toInt()+coord.cap(4).toInt())/2);
      return QString("%1 %2 %3").arg(coord.cap(1).toInt()*100+coord.cap(2).toInt()).
          arg(fitxaBotada).
          arg(coord.cap(3).toInt()*100+coord.cap(4).toInt());
  }
  else {
      return   QString("");}
}

/* fa una pausa en milisegons*/
//void pausa(int temps){
//  QTest::qSleep(temps);
//  qApp->processEvents();
//}
